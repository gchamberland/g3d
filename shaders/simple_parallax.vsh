#version 120

uniform vec3 lightPosition;
uniform vec3 cameraPosition;

attribute vec3 tangent;
attribute vec3 bitangent;

varying vec3 vNormal;
varying vec3 vTangent;
varying vec3 vBinormal;

varying vec3 eyeVec;
varying vec3 lightVec;

void main( void ) {

    gl_TexCoord[0] = gl_TextureMatrix[0] * gl_MultiTexCoord0;

    // Convert vertice, camera, and light vector positions into tangent space
    mat3 TBNMatrix = mat3(tangent, bitangent, gl_Normal);

    eyeVec = (cameraPosition - gl_Vertex.xyz) * TBNMatrix;
    lightVec = (lightPosition - gl_Vertex.xyz) * TBNMatrix;

    gl_Position = ftransform();
}
