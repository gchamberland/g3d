#version 120

varying vec3 lightDir;
varying vec3 normal;
varying vec3 position_cameraSpace;

varying vec3 ac;
varying vec3 dc;
varying vec3 sc;
varying float s;
varying float o;

void main()
{
    vec3 light = normalize(lightDir);
    vec3 normal = normalize(normal);

    float intensity = max(dot(normal, light), 0.0);

    // set the specular term initially to black
    vec3 spec = vec3(0.0);

    if (intensity > 0.0) {
        // compute eye vector and normalize it
        vec3 eye = normalize(-position_cameraSpace);
        // compute the half vector
        vec3 h = normalize(lightDir + eye);

        // compute the specular term into spec
        float intSpec = max(dot(h, normal), 0.0);
        spec = sc * pow(intSpec, s * 100.0);
    }
    // add the specular term
    gl_FragData[0] = vec4(max(intensity *  dc + spec, ac), 1 - o);
}
