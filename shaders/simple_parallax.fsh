#version 120

uniform sampler2D base; // base texture
uniform sampler2D map; // normal map in rgb space, heightmap in alpha channel
uniform int computeLightning;

uniform vec2 scaleBias; // x = scale, y = bias

vec3 ambientColor = vec3(0.98, 0.95, 0.75); // color of ambient light
vec3 diffuseColor = vec3(0.98, 0.95, 0.75); // color of diffuse lighting
vec3 specularColor = vec3(0.98, 0.95, 0.75); // color of specular highlights
float shininess = 30; // how shiny the surface is


varying vec3 tsPosition;
varying vec3 eyeVec;
varying vec3 lightVec;

void main()
{
    // calculate the UV offset
    float height = texture2D(map, gl_TexCoord[0].st).a;
    float v = height * scaleBias.x - scaleBias.y;

    // normalize the camera's tangent space position
    vec3 eye = normalize(eyeVec);

    vec2 newCoords = gl_TexCoord[0].st + (eye.xy * v);

    vec3 color = texture2D(base, newCoords).rgb; // default color

    if (computeLightning != 0)
    {
        vec3 light = normalize(lightVec);

        vec3 normal = texture2D(map, newCoords).rgb * 2.0 - 1.0;

        // calculate lighting values
        float nxDir = max(0.0, dot(normal, light));
        vec3 ambient = ambientColor * color;

        float specularPower = 0.0;
        if(nxDir != 0.0)
        {
            vec3 halfVector = normalize(light + eye);
            float nxHalf = max(0.0, dot(normal, halfVector));
            specularPower = pow(nxHalf, shininess);
        }
        vec3 specular = specularColor * specularPower;

        gl_FragColor = vec4(ambient + (diffuseColor * nxDir * color) + specular, 1.0);
    }
    else
    {
        gl_FragColor = vec4 (color, 1);
    }


}
