#version 120

attribute vec3 normalPerVertex;

varying vec3 normal;

void main()
{
    normal = gl_NormalMatrix * normalPerVertex;

    gl_Position = gl_ModelViewProjectionMatrix * gl_Vertex;
}
