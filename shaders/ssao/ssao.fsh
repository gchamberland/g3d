#version 120

uniform sampler2D normalDepthTexture; // A texture containing the linear depth (alpha) and the fragment's normal in screen space (RGB = xyz).

uniform sampler2D noiseTexture; // A texture containing random rotation coordinates.

uniform vec2 noiseScale; // = vec2(renderTargetWidth / noiseTextureWidth, renderTargetHeight / noiseTextureHeight);

uniform vec3 sampleKernel[32]; // 32 is the maximum number of samples;

uniform int sampleKernelSize = 32; // = number of samples;

uniform float radius = 1.0; // the sampling radius;

varying vec3 viewRay; // A ray from camera to far clip pane.

void main()
{
    //Get the fragment position in view space (x, y, z)
    vec3 origin = viewRay * texture2D(normalDepthTexture, gl_TexCoord[0].st).a;

    //Get the fragment normal.
    vec3 normal = (texture2D(normalDepthTexture, gl_TexCoord[0].st).xyz * 2.0 - 1.0);

    //Get the random rotation vector.
    vec3 rvec = texture2D(noiseTexture, gl_TexCoord[0].st * noiseScale).xyz * 2.0 - 1.0;

    //Compute a change-of-basis matrix to reorient sample kernel along the fragment normal.
    vec3 tangent = normalize(rvec - normal * dot(rvec, normal));
    vec3 bitangent = cross(normal, tangent);
    mat3 TBN = mat3(tangent, bitangent, normal);

    float occlusion = 0.0;

    for (int i=0; i<sampleKernelSize; i++)
    {
        //Get the SAMPLE position in view space (x, y, z) reorient it along the fragment normal.
        vec3 sample = TBN * sampleKernel[i];

        //Scale the sample coordinates by radius and get it position in view space (x, y, z).
        sample = sample * radius + origin;

        //Project the sample position in screen space (x, y).
        vec4 offset = gl_ProjectionMatrix * vec4(sample, 1.0);
        offset.xy /= offset.w;

        //Scale and bias screen space sample position to get it coordinates in depth texture.
        offset.x = offset.x * 0.125 + 0.5;
        offset.y = (offset.y * 0.125) / 1.333 + 0.5;

        //Get the sample depth.
        float sampleDepth = texture2D(normalDepthTexture, offset.xy).a * viewRay.z;

        //Check the depth range.
        float rangeCheck = abs(origin.z - sampleDepth) <= radius ? 1.0 : 0.0;

        //Accumulate the occlusion factor.
        occlusion += (sampleDepth <= sample.z ? 1.0 : .0) * rangeCheck;
    }

    //Normalize occlusion factor.
    occlusion = 1.0 - (occlusion / sampleKernelSize);

    //Store the occlusion factor into the red channel of the output.
    gl_FragColor = vec4(occlusion, 0.0, 0.0, 1.0);
}
