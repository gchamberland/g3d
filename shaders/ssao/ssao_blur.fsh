#version 120

uniform sampler2D rawSSAOResult;
uniform vec2 rawSSAOResultResolution;

uniform sampler2D colorResult;

uniform int blurSize = 4;
uniform int blurSize2 = 16; // = blurSize ^ 2;

uniform vec2 hlim = vec2(-1.5); // =  vec2(float(-blurSize) * 0.5 + 0.5);

void main()
{
    vec2 texelSize = 1.0 / rawSSAOResultResolution;

    float result = 0.0;

    for (int i = 0; i < blurSize; ++i)
    {
        for (int j = 0; j < blurSize; ++j)
        {
            vec2 offset = (hlim + vec2(float(i), float(j))) * texelSize;

            result += texture2D(rawSSAOResult, gl_TexCoord[0].st + offset).r;
        }
    }

    result /= blurSize2;

    gl_FragColor = vec4(result * texture2D(colorResult, gl_TexCoord[0].st).rgb, 1.0);
}
