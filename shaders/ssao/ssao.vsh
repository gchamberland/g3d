#version 120

varying vec3 viewRay;

void main()
{
    //Get the viewRay vector stored in the vertex color data.
    viewRay = gl_Color.rgb;

    gl_TexCoord[0] = gl_Vertex;

    gl_Position = gl_Vertex * 2.0 - 1.0;
}
