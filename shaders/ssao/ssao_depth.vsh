#version 120

attribute vec3 normalPerVertex;

varying float depth;
varying vec3 normal;

void main()
{
    depth = (gl_ModelViewMatrix * gl_Vertex).z;
    depth = -(depth - 1.0) / 99.0;

    normal = (gl_ModelViewProjectionMatrixTranspose * vec4(normalPerVertex, 0.0)).xyz;

    gl_Position = gl_ModelViewProjectionMatrix * gl_Vertex;
}
