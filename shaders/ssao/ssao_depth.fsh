#version 120

varying float depth;
varying vec3 normal;

void main()
{
    gl_FragColor = vec4(normalize(normal) * 0.5 + 0.5, depth);
}
