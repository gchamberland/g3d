#version 120

struct Light
{
    vec3 position;
    vec3 direction;
    vec3 attenuation;
    float radius;
    float power;
    float angle;
    vec3 ambientColor;
    vec3 diffuseColor;
    vec3 specularColor;
};

struct LightInfos
{
    Light lights[1];
    int nbLights;
};

uniform LightInfos lightsInfos;

attribute vec3 ambientColor;
attribute vec3 diffuseColor;
attribute vec3 specularColor;
attribute float shininess;
attribute float opacity;

attribute vec3 normalPerVertex;

varying vec3 lightDir;
varying vec3 normal;
varying vec3 position_cameraSpace;

varying vec3 ac;
varying vec3 dc;
varying vec3 sc;
varying float s;
varying float o;

void main( void ) {

    gl_Position = gl_ModelViewProjectionMatrix * gl_Vertex;

    lightDir = (vec4(lightsInfos.lights[0].position - gl_Vertex.xyz, 1) * gl_ModelViewMatrixInverse).xyz;

    normal = gl_NormalMatrix * normalPerVertex;

    ac = ambientColor * lightsInfos.lights[0].ambientColor;
    dc = diffuseColor * lightsInfos.lights[0].diffuseColor;
    sc = specularColor * lightsInfos.lights[0].specularColor;
    s = shininess;
    o = opacity;

    position_cameraSpace = (gl_Vertex * gl_ModelViewMatrix).xyz;
}
