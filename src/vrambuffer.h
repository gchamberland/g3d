#ifndef VERTEXBUFFEROBJECT_H
#define VERTEXBUFFEROBJECT_H

#define BUFFER_OFFSET(offset)  ((GLvoid*) NULL + offset)

#include <GL/glew.h>

#include <map>
#include <string>
#include <algorithm>
#include <stdexcept>
#include <vector>

#include <iostream>

template <GLuint BUFFER_TYPE>
class GLBuffer
{
public:
    GLBuffer(GLsizei sizeInByte, GLenum usage = GL_STATIC_DRAW)
    {
        m_offset = 0;

        glGenBuffers(1, &m_id);

        glBindBuffer(BUFFER_TYPE, m_id);

            glBufferData(BUFFER_TYPE, sizeInByte, NULL, usage);

        glBindBuffer(BUFFER_TYPE, 0);

        if (glGetError() == GL_OUT_OF_MEMORY)
            throw std::runtime_error("GLBuffer : out of memory !");
    }

    ~GLBuffer()
    {
        glDeleteBuffers(1, &m_id);
    }

    template <typename TYPE>
    void uploadData(const TYPE *data, GLsizei size, const std::string &name = std::string(), int offset = -1)
    {
        if (offset > 0)
            m_offset = offset * sizeof(TYPE);

        GLsizei realSize = size * sizeof(TYPE);

        glBindBuffer(BUFFER_TYPE, m_id);

            glBufferSubData(BUFFER_TYPE, m_offset, realSize, data);

        glBindBuffer(BUFFER_TYPE, 0);

        if (name != std::string())
            m_namedDataOffset.insert(std::make_pair(name, m_offset));

        m_offset += realSize;
    }

    GLint getOffsetByName(const std::string &name) const
    {
        std::map<std::string, GLint>::const_iterator it = m_namedDataOffset.find(name);

        if (it == m_namedDataOffset.end())
            throw std::runtime_error("Aucune donnée ne correspond à ce nom.");

        return it->second;
    }

    GLint getCurrentOffset() const
    {
        return m_offset;
    }

    GLuint getId() const
    {
        return m_id;
    }

private:
     std::map<std::string, GLint> m_namedDataOffset;
     GLuint m_id;
     GLint m_offset;
};


typedef GLBuffer<GL_ARRAY_BUFFER> GLVertexBuffer;
typedef GLBuffer<GL_ELEMENT_ARRAY_BUFFER> GLIndexBuffer;
typedef GLBuffer<GL_UNIFORM_BUFFER> GLUniformBuffer;


class VertexArrayObject
{
public:
    VertexArrayObject()
    {
        glGenVertexArrays(1, &m_Id);

        m_positionsBufferDefined = false;
        m_colorsBufferDefined = false;
        m_texCoordsBufferDefined = false;
        m_normalsBufferDefined = false;
    }

    ~VertexArrayObject()
    {
        glDeleteVertexArrays(1, &m_Id);
    }

    void setPositions(const GLBuffer<GL_ARRAY_BUFFER> *positionsBuffer,
                      GLint nbComponents, GLenum glType,
                      const std::string &offsetName = std::string(),
                      GLsizei stride = 0)
    {
        glBindVertexArray(m_Id);

            glBindBuffer(GL_ARRAY_BUFFER, positionsBuffer->getId());

            if (offsetName.size() > 0)
                glVertexPointer(nbComponents, glType, stride, BUFFER_OFFSET(positionsBuffer->getOffsetByName(offsetName)));
            else
                glVertexPointer(nbComponents, glType, stride, BUFFER_OFFSET(0));

            glBindBuffer(GL_ARRAY_BUFFER, 0);

        glBindVertexArray(0);

        m_positionsBufferDefined = true;
    }

    void setNormals(const GLBuffer<GL_ARRAY_BUFFER> *normalsBuffer,
                    GLenum glType,
                    const std::string &offsetName = std::string(),
                    GLsizei stride = 0)
    {
        glBindVertexArray(m_Id);

            glBindBuffer(GL_ARRAY_BUFFER, normalsBuffer->getId());

            if (offsetName.size() > 0)
                glNormalPointer(glType, stride, BUFFER_OFFSET(normalsBuffer->getOffsetByName(offsetName)));
            else
                glNormalPointer(glType, stride, BUFFER_OFFSET(0));

            glBindBuffer(GL_ARRAY_BUFFER, 0);

        glBindVertexArray(0);

        m_normalsBufferDefined = true;
    }

    void setColors(const GLBuffer<GL_ARRAY_BUFFER> *colorsBuffer,
                   GLint nbComponents, GLenum glType,
                   const std::string &offsetName = std::string(),
                   GLsizei stride = 0)
    {
        glBindVertexArray(m_Id);

            glBindBuffer(GL_ARRAY_BUFFER, colorsBuffer->getId());

            if (offsetName.size() > 0)
                glColorPointer(nbComponents, glType, stride, BUFFER_OFFSET(colorsBuffer->getOffsetByName(offsetName)));
            else
                glColorPointer(nbComponents, glType, stride, BUFFER_OFFSET(0));

            glBindBuffer(GL_ARRAY_BUFFER, 0);

        glBindVertexArray(0);

        m_colorsBufferDefined = true;
    }

    void setTextCoords(const GLBuffer<GL_ARRAY_BUFFER> *texCoordsBuffer,
                       GLint nbComponents, GLenum glType,
                       const std::string &offsetName = std::string(),
                       GLsizei stride = 0)
    {
        glBindVertexArray(m_Id);

            glBindBuffer(GL_ARRAY_BUFFER, texCoordsBuffer->getId());

            if (offsetName.size() > 0)
                glTexCoordPointer(nbComponents, glType, stride, BUFFER_OFFSET(texCoordsBuffer->getOffsetByName(offsetName)));
            else
                glTexCoordPointer(nbComponents, glType, stride, BUFFER_OFFSET(0));

            glBindBuffer(GL_ARRAY_BUFFER, 0);

        glBindVertexArray(0);

        m_texCoordsBufferDefined = true;
    }

    void setAttributs(GLuint attributIndex,
                      const GLBuffer<GL_ARRAY_BUFFER> *attributsBuffer,
                      GLint nbComponents,
                      GLenum glType,
                      const std::string &offsetName = std::string(),
                      GLsizei stride = 0)
    {
        glBindVertexArray(m_Id);

            glBindBuffer(GL_ARRAY_BUFFER, attributsBuffer->getId());

            if (offsetName.size() > 0)
                glVertexAttribPointer(attributIndex, nbComponents, glType, GL_FALSE, stride, BUFFER_OFFSET(attributsBuffer->getOffsetByName(offsetName)));
            else
                glVertexAttribPointer(attributIndex, nbComponents, glType, GL_FALSE, stride, BUFFER_OFFSET(0));

            glBindBuffer(GL_ARRAY_BUFFER, 0);

        glBindVertexArray(0);

        m_attributsIndexes.push_back(attributIndex);
    }

    void draw(GLsizei nbVertices) const
    {
        glBindVertexArray(m_Id);

        glEnableClientState(GL_VERTEX_ARRAY);

        if (m_colorsBufferDefined)
            glEnableClientState(GL_COLOR_ARRAY);

        if (m_texCoordsBufferDefined)
            glEnableClientState(GL_TEXTURE_COORD_ARRAY);

        if (m_normalsBufferDefined)
            glEnableClientState(GL_NORMAL_ARRAY);

        for (std::vector<GLuint>::const_iterator it = m_attributsIndexes.begin(); it != m_attributsIndexes.end(); it++)
            glEnableVertexAttribArray(*it);

        glDrawArrays(GL_TRIANGLES, 0, nbVertices);

        for (std::vector<GLuint>::const_iterator it = m_attributsIndexes.begin(); it != m_attributsIndexes.end(); it++)
            glDisableVertexAttribArray(*it);

        if (m_normalsBufferDefined)
            glDisableClientState(GL_NORMAL_ARRAY);

        if (m_texCoordsBufferDefined)
            glDisableClientState(GL_TEXTURE_COORD_ARRAY);

        if (m_colorsBufferDefined)
            glDisableClientState(GL_COLOR_ARRAY);

        glDisableClientState(GL_VERTEX_ARRAY);

        glBindVertexArray(0);
    }

private:

    GLuint m_Id;

    bool m_positionsBufferDefined;
    bool m_normalsBufferDefined;
    bool m_colorsBufferDefined;
    bool m_texCoordsBufferDefined;
    std::vector<GLuint> m_attributsIndexes;
};

#endif // VERTEXBUFFEROBJECT_H
