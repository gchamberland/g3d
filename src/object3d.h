#ifndef OBJECT3D_H
#define OBJECT3D_H

#include <vector>

#include "drawable.h"
#include "mesh.h"
#include "effect.h"
#include "model3d.h"

class Model3D;

class Object3D
{
public:
    Object3D();

    Object3D(const Object3D &other)
    {
        m_indices = other.m_indices;
        m_positions = other.m_positions;
        m_colors = other.m_colors;
        m_textCoords = other.m_textCoords;
        m_effect = other.m_effect;
    }

    Object3D(const Model3D &owner);

protected:
    std::vector<unsigned int> &indices()
    {
        return m_indices;
    }

    std::vector<unsigned int> &materialsIndices()
    {
        return m_materialIndices;
    }

    std::vector<float> &positions()
    {
        return m_positions;
    }

    std::vector<float> &colors()
    {
        return m_colors;
    }

    std::vector<float> &textCoords()
    {
        return m_textCoords;
    }

private:

   std::vector<unsigned int> m_indices;
   std::vector<unsigned int> m_materialIndices;

   std::vector<float> m_positions;
   std::vector<float> m_colors;
   std::vector<float> m_textCoords;

   G3D::Mesh<float> *m_mesh;
   G3D::Template_Drawable<float> m_drawable;

   G3D::Effect m_effect;

   Model3D *m_owner;

};

#endif // OBJECT3D_H
