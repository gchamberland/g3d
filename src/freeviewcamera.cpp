#include "freeviewcamera.h"

#include <iostream>

#include <cmath>

#include <GL/glu.h>

#include <eigen3/Eigen/Geometry>

namespace G3D
{
    FreeViewCamera::FreeViewCamera(const G3DWidget *widget)
        :PerspectiveCamera(widget)
    {
        m_speed = 0;
        m_lateraleSpeed = 0;

        m_horizontaleRotation = 0;
        m_verticaleRotation =0;
    }


    void FreeViewCamera::forward(double acceleration)
    {
        m_speed += acceleration;
    }

    void FreeViewCamera::backward(double acceleration)
    {
        forward(-acceleration);
    }


    void FreeViewCamera::right(double acceleration)
    {
        m_lateraleSpeed += acceleration;
    }

    void FreeViewCamera::left(double acceleration)
    {
        right(-acceleration);
    }

    void FreeViewCamera::horizontaleRotation(float angle)
    {
        m_horizontaleRotation += angle;
    }

    void FreeViewCamera::verticaleRotation(float angle)
    {
        m_verticaleRotation += angle;
    }

    void FreeViewCamera::update()
    {
        Eigen::Matrix3f r;

        r = Eigen::AngleAxisf(m_horizontaleRotation / 57.3f, Vector3F::UnitY());

        m_lookDirection = r * m_lookDirection;

        r = Eigen::AngleAxisf(m_verticaleRotation / 57.3f, m_lookDirection.cross(Vector3F::UnitY()));

        m_lookDirection = r * m_lookDirection;

        m_lookDirection.normalize();

        m_position += m_lookDirection * m_speed;
        m_position += m_lookDirection.cross(Vector3F::UnitY()) * m_lateraleSpeed;

        updateTargetPoint();

        m_horizontaleRotation = 0;
        m_verticaleRotation = 0;

        m_speed /=  1.5;
        m_lateraleSpeed /= 1.5;

        glMatrixMode(GL_MODELVIEW);

        glLoadIdentity();

        gluLookAt(m_position.x(), m_position.y(), m_position.z(), m_targetPoint.x(), m_targetPoint.y(), m_targetPoint.z(), 0, 1, 0);
    }
}
