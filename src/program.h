#ifndef PROGRAM_H
#define PROGRAM_H

#include <iostream>
#include <stdexcept>
#include <sstream>
#include <cstring>

#include <stdio.h>

#include <map>
#include <vector>

#include <GL/glew.h>
#include "g3dUtils.h"
#include "types/g3dVector.h"
#include "texture.h"

namespace G3D
{
    struct GLSLParam
    {
        GLuint location;
        GLenum type;
        GLint nbElem;
    };

    class Program
    {
    public:
        Program();
        Program(const char *vertexShaderFileName,
               const char *fragmentShaderFileName = NULL,
               const char *geometryShaderFileName = NULL,
               const char *tessellationControlShaderFileName = NULL,
               const char *tessellationEvalutaionShaderFileName = NULL);

        Program(const Program &other);

        ~Program();

        bool hasAttribut(const char *varName) const;
        bool hasUniform(const char *varName) const;

        const GLSLParam * getUniformInfos(const char *varName) const;
        const GLSLParam * getAttributInfos(const char *varName) const;

        GLuint getProgramId() const;

    private:
        GLuint m_id;
        std::vector<GLuint> m_shadersIds;

        std::map<const std::string, GLSLParam> m_attributs;
        std::map<const std::string, GLSLParam> m_uniforms;

        static std::map<GLuint, unsigned int> m_programInstances;

        static GLuint loadShader(GLenum type, const char *filename);
        static char *loadSource(const char *filename);
        static void attachShaders(GLuint programId, const std::vector<GLuint> &shadersIds);

        template <GLenum PARAM_TYPE>
        static void listParams(GLuint programId, std::map<const std::string, GLSLParam> *paramList)
        {
            int total = -1;

            glGetProgramiv( programId, PARAM_TYPE, &total);

            GLSLParam param;

            for (int i=0; i<total; ++i)
            {
                int name_len=-1;
                char name[100];

                switch (PARAM_TYPE)
                {
                    case GL_ACTIVE_UNIFORMS:
                        glGetActiveUniform(programId, GLuint(i), sizeof(name)-1,
                            &name_len, &(param.nbElem), &(param.type), name);
                    break;

                    case GL_ACTIVE_ATTRIBUTES:
                        glGetActiveAttrib(programId, GLuint(i), sizeof(name)-1,
                            &name_len, &(param.nbElem), &(param.type), name);
                    break;
                }

                name[name_len] = 0;

                param.location = glGetUniformLocation(programId, name);

                (*paramList)[std::string(name)] = param;
            }
        }
    };
}

#endif // PROGRAM_H
