#ifndef DRAWABLE_H
#define DRAWABLE_H

#include <eigen3/Eigen/Core>

#include <GL/glew.h>
#include "effect.h"
#include "vrambuffer.h"
#include "mesh.h"
#include "texture.h"

#include "g3dUtils.h"

namespace G3D
{
    class Drawable
    {
    public:
        virtual void draw() const = 0;
        virtual void draw(const Program *effect) const = 0;
        virtual void bindEffect(const Program *effect) = 0;
        virtual void rotate(float x, float y, float z) = 0;
        virtual void translate(float x, float y, float z) = 0;
    };

    template <typename TYPE>
    class Template_Drawable : public Drawable
    {
    public:
        Template_Drawable()
        {

        }

        Template_Drawable(const Mesh<TYPE> *mesh, const Program *defaultEffect = NULL,
                           const Texture *defaultTexture = NULL)
        {
            m_defaultEffect = defaultEffect;
            m_defaultTexture = defaultTexture;

            m_rotationX = 0;
            m_rotationY = 0;
            m_rotationZ = 0;

            m_translationX = 0;
            m_translationY = 0;
            m_translationZ = 0;

            storeDataInVRAM(mesh);
        }

        ~Template_Drawable()
        {
            for (std::map<const Program *, VertexArrayObject *>::iterator it = m_vaos.begin(); it != m_vaos.end(); it++)
            {
                delete it->second;
            }

            delete m_vbo;
        }

        void rotate(float x, float y, float z)
        {
            m_rotationX += x;
            m_rotationY += y;
            m_rotationZ += z;
        }

        void translate(float x, float y, float z)
        {
            m_translationX += x;
            m_translationY += y;
            m_translationZ += z;
        }

        virtual void draw() const
        {
            if (m_defaultEffect == NULL)
            {
                if (m_defaultTexture != NULL)
                    m_defaultTexture->use();

                glMatrixMode(GL_MODELVIEW);
                glPushMatrix();
                glTranslatef(m_translationX, m_translationY, m_translationZ);
                glRotatef(m_rotationX, 1, 0, 0);
                glRotatef(m_rotationY, 0, 1, 0);
                glRotatef(m_rotationZ, 0, 0, 1);

                m_defaultVao.draw(m_nbVertices);

                glPopMatrix();

                if (m_defaultTexture != NULL)
                    m_defaultTexture->useNoTexture();
            }
            else
                draw(m_defaultEffect);
        }

        virtual void draw(const Program *effect) const
        {
            glMatrixMode(GL_MODELVIEW);
            glPushMatrix();
            glTranslatef(m_translationX, m_translationY, m_translationZ);
            glRotatef(m_rotationX, 1, 0, 0);
            glRotatef(m_rotationY, 0, 1, 0);
            glRotatef(m_rotationZ, 0, 0, 1);

            m_vaos.at(effect)->draw(m_nbVertices);

            glPopMatrix();
        }

        virtual void bindEffect(const Program *effect)
        {
            VertexArrayObject *vao = new VertexArrayObject();

            vao->setPositions(m_vbo, 3, GL_TYPE(TYPE), "positions");
            vao->setNormals(m_vbo, GL_TYPE(TYPE), "normalsPerFace");

            if (m_hasColors)
                vao->setColors(m_vbo, 3, GL_TYPE(TYPE), "colors");

            if (m_hasTextCoords)
                vao->setTextCoords(m_vbo, 2, GL_TYPE(TYPE), "textCoords");

            if (m_hasMaterials && effect->hasAttribut("ambientColor"))
            {
                const GLSLParam *param = effect->getAttributInfos("ambientColor");

                if (param->type == GL_FLOAT_VEC3 && param->nbElem == 1)
                    vao->setAttributs(param->location, m_vbo, 3, GL_TYPE(TYPE), "ambientColors");
            }

            if (m_hasMaterials && effect->hasAttribut("diffuseColor"))
            {
                const GLSLParam *param = effect->getAttributInfos("diffuseColor");

                if (param->type == GL_FLOAT_VEC3 && param->nbElem == 1)
                    vao->setAttributs(param->location, m_vbo, 3, GL_TYPE(TYPE), "diffuseColors");
            }

            if (m_hasMaterials && effect->hasAttribut("specularColor"))
            {
                const GLSLParam *param = effect->getAttributInfos("specularColor");

                if (param->type == GL_FLOAT_VEC3 && param->nbElem == 1)
                    vao->setAttributs(param->location, m_vbo, 3, GL_TYPE(TYPE), "specularColors");
            }

            if (m_hasMaterials && effect->hasAttribut("shininess"))
            {
                const GLSLParam *param = effect->getAttributInfos("shininess");

                if (param->type == GL_FLOAT && param->nbElem == 1)
                    vao->setAttributs(param->location, m_vbo, 1, GL_TYPE(TYPE), "shininesses");
            }

            if (m_hasMaterials && effect->hasAttribut("opacity"))
            {
                const GLSLParam *param = effect->getAttributInfos("opacity");

                if (param->type == GL_FLOAT && param->nbElem == 1)
                    vao->setAttributs(param->location, m_vbo, 1, GL_TYPE(TYPE), "opacities");
            }

            if (effect->hasAttribut("tangent"))
                vao->setAttributs(effect->getAttributInfos("tangent")->location, m_vbo, 3, GL_TYPE(TYPE), "tangents");

            if (effect->hasAttribut("bitangent"))
                vao->setAttributs(effect->getAttributInfos("bitangent")->location, m_vbo, 3, GL_TYPE(TYPE), "bitangents");

            if (effect->hasAttribut("normalPerVertex"))
                vao->setAttributs(effect->getAttributInfos("normalPerVertex")->location, m_vbo, 3, GL_TYPE(TYPE), "normalsPerVertex");

            m_vaos[effect] = vao;
        }

    private:
        const Texture *m_defaultTexture;

        std::map<const Program *, VertexArrayObject *> m_vaos;
        const Program *m_defaultEffect;
        VertexArrayObject m_defaultVao;

        GLVertexBuffer *m_vbo;
        size_t m_nbVertices;

        bool m_hasColors;
        bool m_hasTextCoords;
        bool m_hasMaterials;

        float m_rotationX, m_rotationY, m_rotationZ;
        float m_translationX, m_translationY, m_translationZ;

        static void getPositionsPerVertex(const Mesh<TYPE> *mesh, std::vector<TYPE> *positions)
        {
            positions->resize(mesh->getFaces().size() * 9, 0);

            size_t index = 0;

            for (size_t i=0; i<mesh->getFaces().size(); i++)
            {
                for (size_t j=0; j<3; j++)
                {
                    copy<TYPE, 3>(positions->data()+index, mesh->getFaces()[i]->vertices()[j]->positionData());

                    index+=3;
                }
            }
        }

        static void getNormalsPerVertex(const Mesh<TYPE> *mesh, std::vector<TYPE> *normals)
        {
            normals->resize(mesh->getFaces().size() * 9, 0);

            size_t index = 0;

            for (size_t i=0; i<mesh->getFaces().size(); i++)
            {
                for
                        (size_t j=0; j<3; j++)
                {
                    copy<TYPE, 3>(normals->data()+index, mesh->getFaces()[i]->vertices()[j]->getNormal().data());

                    index+=3;
                }
            }
        }

        static void getTangentSpacePerVertex(const Mesh<TYPE> *mesh,
                                             std::vector<TYPE> *normals,
                                             std::vector<TYPE> *tangents,
                                             std::vector<TYPE> *bitangents)
        {
            normals->resize(mesh->getFaces().size() * 9, 0);
            tangents->resize(mesh->getFaces().size() * 9, 0);
            bitangents->resize(mesh->getFaces().size() * 9, 0);

            size_t index = 0;

            for (size_t i=0; i<mesh->getFaces().size(); i++)
            {
                for (size_t j=0; j<3; j++)
                {
                    copy<TYPE, 3>(normals->data()+index, mesh->getFaces()[i]->getNormal().data());
                    copy<TYPE, 3>(tangents->data()+index, mesh->getFaces()[i]->getTangent().data());
                    copy<TYPE, 3>(bitangents->data()+index, mesh->getFaces()[i]->getBitangent().data());

                    index+=3;
                }
            }
        }

        static void getMaterialsPerVertex(const Mesh<TYPE> *mesh,
                                          std::vector<TYPE> *ambientColors,
                                          std::vector<TYPE> *diffuseColors,
                                          std::vector<TYPE> *specularColors,
                                          std::vector<TYPE> *shininesses,
                                          std::vector<TYPE> *opacities)
        {
            ambientColors->resize(mesh->getFaces().size() * 9, 0);
            diffuseColors->resize(mesh->getFaces().size() * 9, 0);
            specularColors->resize(mesh->getFaces().size() * 9, 0);
            shininesses->resize(mesh->getFaces().size() * 3, 0);
            opacities->resize(mesh->getFaces().size() * 3, 0);

            size_t index = 0;
            size_t index2 = 0;

            for (size_t i=0; i<mesh->getFaces().size(); i++)
            {
                for (size_t j=0; j<3; j++)
                {
                    copy<TYPE, 3>(ambientColors->data()+index, (mesh->getFaces()[i]->getMaterial().ambientColor));
                    copy<TYPE, 3>(diffuseColors->data()+index, (mesh->getFaces()[i]->getMaterial().diffuseColor));
                    copy<TYPE, 3>(specularColors->data()+index, (mesh->getFaces()[i]->getMaterial().specularColor));
                    copy<TYPE, 1>(shininesses->data()+index2, &(mesh->getFaces()[i]->getMaterial().shininess));
                    copy<TYPE, 1>(opacities->data()+index2, &(mesh->getFaces()[i]->getMaterial().opacity));

                    index+=3;
                    index2+=1;
                }
            }
        }

        static void getColorsPerVertex(const Mesh<TYPE> *mesh, std::vector<TYPE> *colors)
        {
            colors->resize(mesh->getFaces().size() * 9, 0);

            size_t index = 0;

            for (size_t i=0; i<mesh->getFaces().size(); i++)
            {
                for (size_t j=0; j<3; j++)
                {
                    copy<TYPE, 3>(colors->data()+index, mesh->getFaces()[i]->vertices()[j]->colorData());

                    index+=3;
                }
            }
        }

        static void getTextCoordsPerVertex(const Mesh<TYPE> *mesh, std::vector<TYPE> *textCoords)
        {
            textCoords->resize(mesh->getFaces().size() * 6, 0);

            size_t index = 0;

            for (size_t i=0; i<mesh->getFaces().size(); i++)
            {
                for (size_t j=0; j<3; j++)
                {
                    copy<TYPE, 2>(textCoords->data()+index, mesh->getFaces()[i]->vertices()[j]->textCoordsData());

                    index+=2;
                }
            }
        }

        void storeDataInVRAM(const Mesh<TYPE> *mesh)
        {
            std::vector<TYPE> positions;
            std::vector<TYPE> colors;
            std::vector<TYPE> normalsPerFace;
            std::vector<TYPE> normalsPerVertex;
            std::vector<TYPE> tangents;
            std::vector<TYPE> bitangents;
            std::vector<TYPE> textCoords;
            std::vector<TYPE> ambientColors;
            std::vector<TYPE> diffuseColors;
            std::vector<TYPE> specularColors;
            std::vector<TYPE> shininesses;
            std::vector<TYPE> opacities;

            m_nbVertices = mesh->getFaces().size() * 3;

            m_hasColors = mesh->containsVerticesColors();
            m_hasTextCoords = mesh->containsVerticesTextCoords();
            m_hasMaterials = mesh->containsFacesMaterials();

            getPositionsPerVertex(mesh, &positions);
            getTangentSpacePerVertex(mesh, &normalsPerFace, &tangents, &bitangents);
            getNormalsPerVertex(mesh, &normalsPerVertex);

            if (mesh->containsVerticesColors())
                getColorsPerVertex(mesh, &colors);

            if (mesh->containsVerticesTextCoords())
                getTextCoordsPerVertex(mesh, &textCoords);

            if (mesh->containsFacesMaterials())
                getMaterialsPerVertex(mesh, &ambientColors, &diffuseColors, &specularColors, &shininesses, &opacities);

            m_vbo = new GLVertexBuffer((positions.size() + colors.size()
                                        + normalsPerFace.size() + normalsPerVertex.size()
                                        + tangents.size() + bitangents.size()
                                        + textCoords.size()
                                        + ambientColors.size() + specularColors.size() + diffuseColors.size()
                                        + shininesses.size() + opacities.size()) * sizeof(TYPE));

            m_vbo->uploadData(positions.data(),         positions.size(),       "positions");
            m_vbo->uploadData(normalsPerFace.data(),    normalsPerFace.size(),  "normalsPerFace");
            m_vbo->uploadData(normalsPerVertex.data(),  normalsPerVertex.size(),"normalsPerVertex");
            m_vbo->uploadData(tangents.data(),          tangents.size(),        "tangents");
            m_vbo->uploadData(bitangents.data(),        bitangents.size(),      "bitangents");

            m_defaultVao.setPositions(m_vbo, 3, GL_TYPE(TYPE), "positions");
            m_defaultVao.setNormals(m_vbo, GL_TYPE(TYPE), "normalsPerFace");

            if (colors.size() > 0)
            {
                m_vbo->uploadData(colors.data(), colors.size(), "colors");
                m_defaultVao.setColors(m_vbo, 3, GL_TYPE(TYPE), "colors");
            }

            if (textCoords.size() > 0)
            {
                m_vbo->uploadData(textCoords.data(), textCoords.size(), "textCoords");
                m_defaultVao.setTextCoords(m_vbo, 2, GL_TYPE(TYPE), "textCoords");
            }

            if (ambientColors.size() > 0)
                m_vbo->uploadData(ambientColors.data(), ambientColors.size(), "ambientColors");

            if (diffuseColors.size() > 0)
                m_vbo->uploadData(diffuseColors.data(), diffuseColors.size(), "diffuseColors");

            if (specularColors.size() > 0)
                m_vbo->uploadData(specularColors.data(), specularColors.size(), "specularColors");

            if (shininesses.size() > 0)
                m_vbo->uploadData(shininesses.data(), shininesses.size(), "shininesses");

            if (opacities.size() > 0)
                m_vbo->uploadData(opacities.data(), opacities.size(), "opacities");

            if (m_defaultEffect != NULL)
                bindEffect(m_defaultEffect);
        }
    };
}

#endif // DRAWABLE_H
