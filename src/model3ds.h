#ifndef MODEL3DS_H
#define MODEL3DS_H

#include <mesh.h>
#include <drawable.h>

#include <lib3ds/mesh.h>

namespace G3D
{
    class Model3DS
    {
    public:
        Model3DS(const char *fileName);
        Drawable *getDrawableModel(const Program *effect = NULL);

    private:
        static Mesh<float> lib3dsMesh2G3DMesh(const Lib3dsMesh *mesh);
        std::vector<float> m_positions;
        std::vector<float> m_textCoords;
        std::vector<Material<float> > m_materials;
        std::vector<GLuint> m_indices;
        std::vector<GLuint> m_facesMaterialIndices;

        Drawable *m_drawable;
    };
}

#endif // MODEL3DS_H
