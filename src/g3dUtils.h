#ifndef G3DUTILS_H
#define G3DUTILS_H

#include <GL/glew.h>

#include <eigen3/Eigen/Core>
#include <eigen3/Eigen/Geometry>

#include "types/g3dmatrix.h"
#include "texture.h"

#define TYPE_ERROR Type_non_repertorie_!

template<bool B, int T, int F>
struct conditional_int { static const int value = T; };

template<int T, int F>
struct conditional_int<false, T, F> { static const int value = F; };

template<class T, class U>
struct is_same_type { static const bool value = false; };

template<class T>
struct is_same_type<T, T> { static const bool value = true; };

template<int T, int U>
struct is_same_int_value { static const bool value = false; };

template<int T>
struct is_same_int_value<T, T> { static const bool value = true; };

template<GLenum T, GLenum U>
struct is_same_enum_value { static const bool value = false; };

template<GLenum T>
struct is_same_enum_value<T, T> { static const bool value = true; };

#define GL_TYPE(TYPE)   conditional_int< is_same_type<TYPE, char>::value, GL_BYTE,                           \
                            conditional_int< is_same_type<TYPE, int>::value, GL_INT,                         \
                                conditional_int< is_same_type<TYPE, float>::value, GL_FLOAT,                 \
                                    conditional_int<is_same_type<TYPE, double>::value, GL_DOUBLE, -1         \
                                        >::value >::value >::value >::value


template <typename TYPE> struct glsl_type { static const GLenum value = GL_FALSE; };
template <> struct glsl_type<unsigned int> { static const GLenum value = GL_UNSIGNED_INT; };
template <> struct glsl_type<int> { static const GLenum value = GL_INT; };
template <> struct glsl_type<float> { static const GLenum value = GL_FLOAT; };
template <> struct glsl_type<double> { static const GLenum value = GL_DOUBLE; };
template <> struct glsl_type<typename G3D::Matrix<float, 2, 1>::type> { static const GLenum value = GL_FLOAT_VEC2; };
template <> struct glsl_type<typename G3D::Matrix<float, 3, 1>::type> { static const GLenum value = GL_FLOAT_VEC3; };
template <> struct glsl_type<typename G3D::Matrix<float, 4, 1>::type> { static const GLenum value = GL_FLOAT_VEC4; };
template <> struct glsl_type<typename G3D::Matrix<float, 2, 2>::type> { static const GLenum value = GL_FLOAT_MAT2; };
template <> struct glsl_type<typename G3D::Matrix<float, 3, 3>::type> { static const GLenum value = GL_FLOAT_MAT3; };
template <> struct glsl_type<typename G3D::Matrix<float, 4, 4>::type> { static const GLenum value = GL_FLOAT_MAT4; };
template <> struct glsl_type<typename G3D::Matrix<float, 2, 3>::type> { static const GLenum value = GL_FLOAT_MAT2x3; };
template <> struct glsl_type<typename G3D::Matrix<float, 3, 2>::type> { static const GLenum value = GL_FLOAT_MAT3x2; };
template <> struct glsl_type<typename G3D::Matrix<float, 2, 4>::type> { static const GLenum value = GL_FLOAT_MAT2x4; };
template <> struct glsl_type<typename G3D::Matrix<float, 4, 2>::type> { static const GLenum value = GL_FLOAT_MAT4x2; };
template <> struct glsl_type<typename G3D::Matrix<float, 3, 4>::type> { static const GLenum value = GL_FLOAT_MAT3x4; };
template <> struct glsl_type<typename G3D::Matrix<float, 4, 3>::type> { static const GLenum value = GL_FLOAT_MAT4x3; };
template <> struct glsl_type<typename G3D::Matrix<double, 2, 1>::type> { static const GLenum value = GL_DOUBLE_VEC2; };
template <> struct glsl_type<typename G3D::Matrix<double, 3, 1>::type> { static const GLenum value = GL_DOUBLE_VEC3; };
template <> struct glsl_type<typename G3D::Matrix<double, 4, 1>::type> { static const GLenum value = GL_DOUBLE_VEC4; };
template <> struct glsl_type<typename G3D::Matrix<double, 2, 2>::type> { static const GLenum value = GL_DOUBLE_MAT2; };
template <> struct glsl_type<typename G3D::Matrix<double, 3, 3>::type> { static const GLenum value = GL_DOUBLE_MAT3; };
template <> struct glsl_type<typename G3D::Matrix<double, 4, 4>::type> { static const GLenum value = GL_DOUBLE_MAT4; };
template <> struct glsl_type<typename G3D::Matrix<double, 2, 3>::type> { static const GLenum value = GL_DOUBLE_MAT2x3; };
template <> struct glsl_type<typename G3D::Matrix<double, 3, 2>::type> { static const GLenum value = GL_DOUBLE_MAT3x2; };
template <> struct glsl_type<typename G3D::Matrix<double, 2, 4>::type> { static const GLenum value = GL_DOUBLE_MAT2x4; };
template <> struct glsl_type<typename G3D::Matrix<double, 4, 2>::type> { static const GLenum value = GL_DOUBLE_MAT4x2; };
template <> struct glsl_type<typename G3D::Matrix<double, 3, 4>::type> { static const GLenum value = GL_DOUBLE_MAT3x4; };
template <> struct glsl_type<typename G3D::Matrix<double, 4, 3>::type> { static const GLenum value = GL_DOUBLE_MAT4x3; };
template <> struct glsl_type<Texture> { static const GLenum value = GL_SAMPLER_2D; };

template <typename TYPE>
struct Material
{
    TYPE ambientColor[3];
    TYPE diffuseColor[3];
    TYPE specularColor[3];
    TYPE shininess;
    TYPE opacity;
};

template <typename TYPE>
struct Light
{
    TYPE position[3];
    TYPE direction[3];
    TYPE ambientColor[3];
    TYPE diffuseColor[3];
    TYPE specularColor[3];
    TYPE attenuation[3];
    TYPE radius;
    TYPE power;
    TYPE angle;
};

template <typename TYPE, int SIZE>
void copy(TYPE *dst, const TYPE *src)
{
    memcpy(dst, src, SIZE * sizeof(TYPE));
}

template <typename TYPE>
TYPE frand(TYPE min, TYPE max)
{
    return min + static_cast <TYPE> (rand()) / ( static_cast <TYPE> (RAND_MAX / (max - min)));
}

template <typename TYPE>
TYPE lerp(TYPE start, TYPE end, float percent)
{
     return (start + percent*(end - start));
}

#endif // G3DUTILS_H
