#include "model3ds.h"

#include <stdexcept>
#include <sstream>
#include <iostream>
#include <map>

#include <lib3ds/io.h>
#include <lib3ds/file.h>
#include <lib3ds/mesh.h>
#include <lib3ds/material.h>
#include <lib3ds/types.h>
#include <lib3ds/chunk.h>
#include <lib3ds/vector.h>
#include <lib3ds/matrix.h>

using namespace G3D;

Model3DS::Model3DS(const char *fileName)
{
    Lib3dsFile *file = lib3ds_file_load(fileName);

    if (file == NULL)
    {
        std::stringstream str;

        str << "Impossible de charger le fichier " << fileName;

        throw std::runtime_error(str.str());
    }

    std::map<const std::string, GLuint > materialsIndex;
    GLuint materialsIndices = 0;

    for (Lib3dsMaterial *material = file->materials; material != NULL; material=material->next)
    {
        Material<float> g3dMaterial;

        std::cout << material->transparency << std::endl;

        memcpy(g3dMaterial.ambientColor, material->ambient, 3 * sizeof(float));
        memcpy(g3dMaterial.diffuseColor, material->diffuse, 3 * sizeof(float));
        memcpy(g3dMaterial.specularColor, material->specular, 3 * sizeof(float));
        memcpy(&g3dMaterial.shininess, &material->shininess, 1 * sizeof(float));
        memcpy(&g3dMaterial.opacity, &material->transparency, 1 * sizeof(float));

        m_materials.push_back(g3dMaterial);
        materialsIndex[std::string(material->name)] = materialsIndices;
        materialsIndices++;
    }

    GLuint pointsOffset = 0;

    for (Lib3dsMesh *mesh = file->meshes; mesh != NULL; mesh = mesh->next)
    {
        for (int i=0; i<mesh->points; i++)
        {
            m_positions.push_back(mesh->pointL[i].pos[0]);
            m_positions.push_back(mesh->pointL[i].pos[1]);
            m_positions.push_back(mesh->pointL[i].pos[2]);
        }

        for (int i=0; i<mesh->texels; i++)
        {
            m_textCoords.push_back(mesh->texelL[i][0]);
            m_textCoords.push_back(mesh->texelL[i][1]);
        }

        for (int i=0; i<mesh->faces; i++)
        {
            m_indices.push_back(mesh->faceL[i].points[0] + pointsOffset);
            m_indices.push_back(mesh->faceL[i].points[1] + pointsOffset);
            m_indices.push_back(mesh->faceL[i].points[2] + pointsOffset);

            m_facesMaterialIndices.push_back(materialsIndex.at(std::string(mesh->faceL[i].material)));
        }

        pointsOffset = m_positions.size() / 3;

        break;
    }
}

Drawable *Model3DS::getDrawableModel(const Program *effect)
{
    return new Template_Drawable<float>(new Mesh<float>(&m_indices,
                                                        &m_positions,
                                                        NULL,
                                                        (m_textCoords.size() > 0 ? &m_textCoords : NULL),
                                                        &m_materials,
                                                        &m_facesMaterialIndices), effect);
}
