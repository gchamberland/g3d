#ifndef PERSPECTIVECAMERA_H
#define PERSPECTIVECAMERA_H

#include <QObject>

#include "g3dUtils.h"
#include "context3d.h"
#include "types/g3dVector.h"

namespace G3D
{
    class G3DWidget;

    class PerspectiveCamera : public QObject
    {
        Q_OBJECT

    public:
        PerspectiveCamera(const G3DWidget *widget, float fovy = 60.0f, float zNear = 1.0f, float zFar = 100.0f);

        PerspectiveCamera(const G3DWidget *widget, float x, float y, float z,
                          float targetPointX, float targetPointY, float targetPointZ,
                          float fovy = 60.0f, float zNear = 1.0f, float zFar = 100.0f);

        PerspectiveCamera(const G3DWidget *widget, const Vector3F &position, const Vector3F &targetPoint,
                          float fovy = 60.0f, float zNear = 1.0f, float zFar = 100.0f);

        PerspectiveCamera(const PerspectiveCamera &other);

        void setPosition(float x, float y, float z);
        void setPosition(const Vector3F &newPosition);

        float getX() const;
        float getY() const;
        float getZ() const;

        const Vector3F &getPosition() const;

        void setTargetPoint(float x, float y, float z);
        void setTargetPoint(const Vector3F &newTargetPoint);

        float getTargetPointX() const;
        float getTargetPointY() const;
        float getTargetPointZ() const;

        const Vector3F &getTargetPoint() const;

        void setLookDirection(float x, float y, float z);
        void setLookDirection(const Vector3F &newLookDirection);

        float getLookDirectionX() const;
        float getLookDirectionY() const;
        float getLookDirectionZ() const;

        const Vector3F &getLookDirection() const;

        float getFovy();
        float getZNear();
        float getZFar();

        virtual void update() const;

    public slots:
        void sizeChangedSlot(int width, int height);

    protected:
        float m_fovy;
        float m_zFar;
        float m_zNear;
        Vector3F m_position;
        Vector3F m_targetPoint;
        Vector3F m_lookDirection;

        void updateLookDirection();
        void updateTargetPoint();

    private:
        void connectSlots(const G3DWidget *widget);
    };
}

#endif // PERSPECTIVECAMERA_H
