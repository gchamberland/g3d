#ifndef G3DMATRIX_H
#define G3DMATRIX_H

#include <eigen3/Eigen/Core>
#include <eigen3/Eigen/Geometry>

namespace G3D
{
    template <typename TYPE, int N_ROWS, int N_COLS>
    struct Matrix
    {
        typedef Eigen::Matrix<TYPE, N_ROWS, N_COLS> type;
        typedef Eigen::Map<Eigen::Matrix<TYPE, N_ROWS, N_COLS> > maptype;
    };
}

#endif // G3DMATRIX_H
