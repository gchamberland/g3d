#ifndef G3DVECTOR_H
#define G3DVECTOR_H

#include "g3dmatrix.h"

namespace G3D
{
    template <typename TYPE, int N>
    struct Vector
    {
        typedef typename Matrix<TYPE, N, 1>::type type;
    };

    template <typename TYPE, int N>
    struct Point
    {
        typedef typename Matrix<TYPE, N, 1>::type type;
    };

    typedef Vector<int, 2>::type Vector2I;
    typedef Vector<float, 2>::type Vector2F;
    typedef Vector<double, 2>::type Vector2D;

    typedef Vector<int, 3>::type Vector3I;
    typedef Vector<float, 3>::type Vector3F;
    typedef Vector<double, 3>::type Vector3D;

    typedef Vector2I Point2I;
    typedef Vector2F Point2F;
    typedef Vector2D Point2D;

    typedef Vector3I Point3I;
    typedef Vector3F Point3F;
    typedef Vector3D Point3D;
}

#endif // G3DVECTOR_H
