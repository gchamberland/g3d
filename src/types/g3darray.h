#ifndef G3DARRAY_H
#define G3DARRAY_H

#include <vector>
#include <stdexcept>

#include "g3dUtils.h"
#include "g3dmatrix.h"

namespace G3D
{
    template <typename TYPE, unsigned int N_ROWS, unsigned int N_COLS>
    class MatrixArray
    {
    public:
        MatrixArray()
        {
            m_size = 0;
        }

        MatrixArray(unsigned int size)
        {
            m_size = size;
            createInterfaces();
        }

        MatrixArray(const MatrixArray<TYPE, N_ROWS, N_COLS> &other)
        {
            m_size = other.m_size;
            createInterfaces();
            memcpy(m_data.data(), other.m_data.data(), N_ROWS * N_COLS * m_size * sizeof(TYPE));
        }

        ~MatrixArray()
        {
            for (unsigned int i=0; i<m_size; i++)
            {
                delete m_interfaces[i];
            }
        }

        typename Matrix<TYPE, N_ROWS, N_COLS>::maptype &at(unsigned int index)
        {
            if (index >= m_size)
                throw std::runtime_error("MatrixArray::at() : index out of bound !");

            return *m_interfaces[index];
        }

        typename Matrix<TYPE, N_ROWS, N_COLS>::maptype &operator[] (unsigned int index)
        {
            return at(index);
        }

        unsigned int size() const
        {
            return m_size;
        }

        const TYPE *data() const
        {
            return m_data.data();
        }

    private:
        unsigned int m_size;
        std::vector<TYPE> m_data;
        std::vector<typename G3D::Matrix<TYPE, N_ROWS, N_COLS>::maptype *> m_interfaces;

        void createInterfaces()
        {
            m_data.resize(N_ROWS * N_COLS * m_size);

            for (unsigned int i=0; i<m_size; i++)
            {
                m_interfaces.push_back(new typename G3D::Matrix<TYPE, N_ROWS, N_COLS>::maptype(m_data.data() + N_ROWS * N_COLS * i));
            }
        }
    };


}

#endif // G3DARRAY_H
