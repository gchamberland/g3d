#include "texture.h"

#include <stdexcept>
#include <sstream>
#include <iostream>

#include <GL/gl.h>

#include <QGLWidget>
#include <QImage>

Texture::Texture()
{
    m_id = 0;
}

Texture::Texture(const Texture &other)
{
    if (other.m_id != 0)
    {
        m_id = other.m_id;
        m_textureInstances[m_id]++;
    }
}

Texture::~Texture()
{
    m_textureInstances[m_id]--;

    if (m_textureInstances[m_id] == 0)
        glDeleteTextures(1, &m_id);
}

Texture::Texture(const char *fileName)
{
    QImage imageTmp(QString::fromUtf8(fileName));

    if (imageTmp.isNull())
    {
        std::stringstream str;

        str << "Impossible de charger le fichier \"" << fileName << "\" :\n";

        throw std::runtime_error(str.str());
    }

    QImage image = QGLWidget::convertToGLFormat(imageTmp);

    glGenTextures(1, &m_id);

    m_textureInstances[m_id] = 1;

    glBindTexture(GL_TEXTURE_2D, m_id);

    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, image.width(), image.height(), 0, GL_RGBA, GL_UNSIGNED_BYTE, image.bits());

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

    glBindTexture(GL_TEXTURE_2D, 0);
}

Texture::Texture(int width, int height, GLenum format, GLenum internalFormat, GLenum glType, float *data)
{
    glGenTextures(1, &m_id);

    m_textureInstances[m_id] = 1;

    glBindTexture(GL_TEXTURE_2D, m_id);

        glTexImage2D(GL_TEXTURE_2D,
                     0,
                     internalFormat,
                     width,
                     height,
                     0,
                     format,
                     glType,
                     data);

        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);

        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

        glGenerateMipmap(GL_TEXTURE_2D);

        glBindTexture(GL_TEXTURE_2D, 0);
}

void Texture::use(unsigned int textureUnitIndex) const
{
    glEnable(GL_TEXTURE_2D);
    glActiveTexture(GL_TEXTURE0 + textureUnitIndex);
    glBindTexture(GL_TEXTURE_2D, m_id);
}

void Texture::useNoTexture(unsigned int textureUnitIndex)
{
    glActiveTexture(GL_TEXTURE0 + textureUnitIndex);
    glBindTexture(GL_TEXTURE_2D, 0);
    glDisable(GL_TEXTURE_2D);
}
