#ifndef MESH_H
#define MESH_H

#include <cmath>
#include <vector>
#include <stdexcept>
#include <iostream>
#include <map>

#include <eigen3/Eigen/Core>
#include <eigen3/Eigen/Geometry>

#include "g3dUtils.h"
#include "types/g3dVector.h"

namespace G3D
{
    template <typename TYPE> class Face;
    template <typename TYPE> class Edge;

    template <typename TYPE>
    class Vertex
    {
    public:
        Vertex(GLuint index, const TYPE *verticesCoords, const TYPE *verticesColors = NULL, const TYPE *verticesTextCoords = NULL)
        {
            m_index = index;

            m_x = &verticesCoords[m_index * 3];
            m_y = &verticesCoords[m_index * 3 + 1];
            m_z = &verticesCoords[m_index * 3 + 2];

            if (verticesColors != NULL)
            {
                m_r = &verticesColors[m_index * 3];
                m_g = &verticesColors[m_index * 3 + 1];
                m_b = &verticesColors[m_index * 3 + 2];
            }
            else
                m_r = m_g = m_b = NULL;

            if (verticesTextCoords != NULL)
            {
                m_s = &verticesTextCoords[m_index * 2];
                m_t = &verticesTextCoords[m_index * 2 + 1];
                m_hasTextCoords = true;
            }
            else
                m_hasTextCoords = false;

            m_normal = NULL;
        }

        ~Vertex()
        {
            if (m_normal != NULL)
                delete m_normal;
        }

        const TYPE *positionData() const
        {
            return m_x;
        }

        const TYPE *colorData() const
        {
            return m_r;
        }

        const TYPE *textCoordsData() const
        {
            return m_s;
        }

        void addFace(Face<TYPE> *face)
        {
            m_faces.push_back(face);
        }

        const TYPE getX() const
        {
            return *m_x;
        }

        const TYPE getY() const
        {
            return *m_y;
        }

        const TYPE getZ() const
        {
            return *m_z;
        }

        const TYPE getR() const
        {
            return *m_r;
        }

        const TYPE getG() const
        {
            return *m_g;
        }

        const TYPE getB() const
        {
            return *m_b;
        }

        const TYPE getS() const
        {
            return *m_s;
        }

        const TYPE getT() const
        {
            return *m_t;
        }

        GLuint getIndex() const
        {
            return m_index;
        }

        const std::vector<Face<TYPE> *> &getFaces() const
        {
            return m_faces;
        }

        const typename Vector<TYPE, 3>::type &getNormal()
        {
            if (m_normal == NULL)
                computeNormal();

            return *m_normal;
        }

        const bool hasTextCoords()
        {
            return m_hasTextCoords;
        }

    protected:
        const TYPE *m_x, *m_y, *m_z;
        const TYPE *m_s, *m_t;
        const TYPE *m_r, *m_g, *m_b;
        std::vector<Face<TYPE> *> m_faces;
        typename Vector<TYPE, 3>::type *m_normal;
        GLuint m_index;
        bool m_hasTextCoords;

    private:
        void computeNormal()
        {
            m_normal = new typename Vector<TYPE, 3>::type();
            *m_normal = Vector<TYPE, 3>::type::Constant(0);

            for (int j=0; j<m_faces.size(); j++)
            {
                *m_normal += m_faces[j]->getNormal();
            }

            m_normal->normalize();
        }
    };

    template <typename TYPE>
    class Edge
    {
    public:
        Edge(Vertex<TYPE> *v1, Vertex<TYPE> *v2, Face<TYPE> *f1 = NULL, Face<TYPE> *f2 = NULL)
        {
            m_v1 = v1;
            m_v2 = v2;
            m_f1 = f1;
            m_f2 = f2;
        }

        void setF1(Face<TYPE> *face)
        {
            if (face == m_f2)
                throw std::runtime_error("Une arrete ne peut pas avoir deux fois la meme face voisine.");

            m_f1 = face;
        }

        void setF2(Face<TYPE> *face)
        {
            if (face == m_f1)
                throw std::runtime_error("Une arrete ne peut pas avoir deux fois la meme face voisine.");

            m_f2 = face;
        }

        const Face<TYPE> *getF1() const
        {
            return m_f1;
        }

        const Face<TYPE> *getF2() const
        {
            return m_f2;
        }

        const Vertex<TYPE> *getV1() const
        {
            return m_v1;
        }

        const Vertex<TYPE> *getV2() const
        {
            return m_v2;
        }

        const Face<TYPE> *getOtherFace(const Face<TYPE> *face)
        {
            if (face == NULL)
                throw std::runtime_error("Face is null.");

            if (face != m_f1 && face != m_f2)
                throw std::runtime_error("Not a face from this edge.");

            if (face == m_f1)
                return m_f2;
            else
                return m_f1;
        }

        Vertex<TYPE> *m_v1, *m_v2;
        Face<TYPE> *m_f1, *m_f2;
    };

    template <typename TYPE>
    class Face
    {
    public:
        Face(Vertex<TYPE> *v1, Vertex<TYPE> *v2, Vertex<TYPE> *v3,
             Edge<TYPE> *e1, Edge<TYPE> *e2, Edge<TYPE> *e3, const Material<TYPE> *faceMaterial = NULL)
        {
            m_vertices.push_back(v1);
            m_vertices.push_back(v2);
            m_vertices.push_back(v3);

            m_edges.push_back(e1);
            m_edges.push_back(e2);
            m_edges.push_back(e3);

            m_material = faceMaterial;

            m_lighted = false;

            computeTangentSpaceBasis();
        }

        const Edge<TYPE> *getE1() const
        {
            return m_edges[0];
        }

        const Edge<TYPE> *getE2() const
        {
            return m_edges[1];
        }

        const Edge<TYPE> *getE3() const
        {
            return m_edges[2];
        }

        const Vertex<TYPE> *getV1() const
        {
            return m_vertices[0];
        }

        const Vertex<TYPE> *getV2() const
        {
            return m_vertices[1];
        }

        const Vertex<TYPE> *getV3() const
        {
            return m_vertices[2];
        }

        const std::vector<Edge<TYPE> *> &edges() const
        {
            return m_edges;
        }

        const std::vector<Vertex<TYPE> *> &vertices() const
        {
            return m_vertices;
        }

        const typename Vector<TYPE, 3>::type &getNormal() const
        {
            return m_normal;
        }

        const typename Vector<TYPE, 3>::type &getTangent() const
        {
            return m_tangent;
        }

        const typename Vector<TYPE, 3>::type &getBitangent() const
        {
            return m_bitangent;
        }

        const Material<TYPE> &getMaterial() const
        {
            return *m_material;
        }

        bool setLightedFrom(const Point3D &lightPos)
        {
            typename Vector<TYPE, 3>::type lightDirection = Point3D(m_vertices[0]->positionData()) - lightPos +
                                    Point3D(m_vertices[1]->positionData()) - lightPos +
                                      Point3D(m_vertices[2]->positionData()) - lightPos / 3;

            lightDirection.normalize();

            m_lighted = lightDirection.dot(m_normal) < 0;
        }

        bool isLighted() const
        {
            return m_lighted;
        }

    protected:
        typename Vector<TYPE, 3>::type m_normal;
        typename Vector<TYPE, 3>::type m_tangent;
        typename Vector<TYPE, 3>::type m_bitangent;
        std::vector<Edge<TYPE> *> m_edges;
        std::vector<Vertex<TYPE> *> m_vertices;
        const Material<TYPE> *m_material;

    private:
        void computeTangentSpaceBasis()
        {
            typename Point<TYPE, 3>::type p1(m_vertices[0]->positionData());
            typename Point<TYPE, 3>::type p2(m_vertices[1]->positionData());
            typename Point<TYPE, 3>::type p3(m_vertices[2]->positionData());

            m_normal = (p2 - p1).cross(p3 - p1);
            m_normal.normalize();

            if (m_vertices[0]->hasTextCoords())
            {
                typename Vector<TYPE, 3>::type vPos1_2 = p2 - p1;
                typename Vector<TYPE, 3>::type vPos1_3 = p3 - p1;

                typename Vector<TYPE, 2>::type vText1_2 = typename Point<TYPE, 2>::type(m_vertices[1]->textCoordsData()) -
                                                            typename Point<TYPE, 2>::type(m_vertices[0]->textCoordsData());

                typename Vector<TYPE, 2>::type vText1_3 = typename Point<TYPE, 2>::type(m_vertices[2]->textCoordsData()) -
                                                            typename Point<TYPE, 2>::type(m_vertices[0]->textCoordsData());

                TYPE r = ((TYPE)1) / (vText1_2.x() * vText1_3.y() - vText1_2.y() * vText1_3.x());
                m_tangent = (vPos1_2 * vText1_3.y() - vPos1_3 * vText1_2.y()) * r;
                m_tangent.normalize();
                m_bitangent = (vPos1_3 * vText1_2.x() - vPos1_2 * vText1_3.x()) * r;
            }
            else
            {
                m_tangent = p2 - p1;
                m_tangent.normalize();
                m_bitangent = m_normal.cross(m_tangent);
            }

            m_bitangent.normalize();
        }

        bool m_lighted;
    };

    template <typename TYPE>
    class Mesh
    {
    public:
        Mesh()
        {
            m_containsVerticesTextCoords = false;
            m_containsVerticesColors = false;
            m_containsFacesMaterials = false;
        }

        Mesh(const std::vector<GLuint> *indices,
             const std::vector<TYPE> *positions,
             const std::vector<TYPE> *colors = NULL,
             const std::vector<TYPE> *textCoords = NULL,
             const std::vector<Material<TYPE> > *facesMaterials = NULL,
             const std::vector<GLuint> *facesMaterialsIndices = NULL)
        {
            if (positions->size() % 3 != 0)
                throw std::runtime_error("Erreur : positions->size() % 3 != 0");

            if (*std::max_element(indices->begin(), indices->end()) > positions->size() / 3 - 1)
                throw std::runtime_error("Erreur : max_element(indices) > positions->size() / 3 - 1");

            if (colors != NULL)
                if (colors->size() != positions->size())
                    throw std::runtime_error("Erreur : colors->size() != positions->size()");

            if (textCoords != NULL)
                if (textCoords->size() / 2 != positions->size() / 3)
                    throw std::runtime_error("Erreur : textCoords->size() / 2 != positions->size() / 3");

            if (facesMaterials != NULL && facesMaterialsIndices != NULL)
            {
                if (*std::max_element(facesMaterialsIndices->begin(), facesMaterialsIndices->end()) > facesMaterials->size() - 1)
                    throw std::runtime_error("Erreur : max_element(facesMaterialsIndices) > materials->size() - 1");

                if (facesMaterialsIndices->size() != indices->size() / 3)
                    throw std::runtime_error("Erreur : facesMaterialsIndices->size() != indices->size() / 3");
            }

            allocVectors(positions->size() / 3);

            for (int i=0; i<indices->size(); i+=3)
            {
                createFace((*indices)[i], (*indices)[i+1], (*indices)[i+2],
                           positions->data(),
                           colors != NULL ? colors->data() : NULL,
                           textCoords != NULL ? textCoords->data() : NULL,
                           facesMaterials != NULL ? facesMaterials->data() : NULL,
                           facesMaterialsIndices != NULL ? facesMaterialsIndices->data() : NULL);
            }

            m_containsFacesMaterials = facesMaterials != NULL;
            m_containsVerticesColors = colors != NULL;
            m_containsVerticesTextCoords = textCoords != NULL;
        }

        ~Mesh()
        {
            for (int i=0; i<m_faces.size(); ++i)
                delete m_faces[i];

            for (typename std::map<std::pair<GLuint, GLuint>, Edge<TYPE> *>::iterator it = m_edges.begin();
                 it != m_edges.end; it++)
                delete it->second;

            for (int i=0; i<m_vertices.size(); ++i)
                delete m_vertices[i];
        }

        void setLightPosition(const Point3D &lightPos)
        {
            for (int i=0; i<m_faces.size(); ++i)
                m_faces[i]->setLightedFrom(lightPos);
        }

        const std::vector<Face<TYPE> *> &getFaces() const
        {
            return m_faces;
        }

        const std::vector<Edge<TYPE> *> &getEdges() const
        {
            return m_edges;
        }

        const std::vector<Vertex<TYPE> *> &getVertices() const
        {
            return m_vertices;
        }

        const bool containsFacesMaterials() const
        {
            return m_containsFacesMaterials;
        }

        const bool containsVerticesColors() const
        {
            return m_containsVerticesColors;
        }

        const bool containsVerticesTextCoords() const
        {
            return m_containsVerticesTextCoords;
        }

    private:
        void allocVectors(int nbVertices)
        {
            for (int i=0; i<nbVertices; ++i)
            {
                m_vertices.push_back(NULL);
            }
        }

        Vertex<TYPE> *createVertex(GLuint index, const TYPE *verticesCoords,
                                   const TYPE *verticesColors = NULL, const TYPE *verticesTextCoords = NULL)
        {
            if (m_vertices[index] == NULL)
            {
                m_vertices[index] = new Vertex<TYPE>(index, verticesCoords, verticesColors, verticesTextCoords);
            }

            return m_vertices[index];
        }

        Edge<TYPE> *createEdge(GLuint indexV1, GLuint indexV2, const TYPE *verticesCoords,
                               const TYPE *verticesColors = NULL, const TYPE *verticesTextCoords = NULL)
        {
            if (m_edges.find(std::make_pair(indexV1, indexV2)) == m_edges.end())
            {
                Vertex<TYPE> *v1 = createVertex(indexV1, verticesCoords, verticesColors, verticesTextCoords);
                Vertex<TYPE> *v2 = createVertex(indexV2, verticesCoords, verticesColors, verticesTextCoords);

                m_edges[std::make_pair(indexV1, indexV2)] = new Edge<TYPE>(v1, v2);
            }

            return m_edges.at(std::make_pair(indexV1, indexV2));
        }

        void createFace(GLuint indexV1, GLuint indexV2, GLuint indexV3, const TYPE *verticesCoords,
                        const TYPE *verticesColors = NULL, const TYPE *verticesTextCoords = NULL,
                        const Material<TYPE> *facesMaterials = NULL, const GLuint *facesMaterialsIndices = NULL)
        {
            Edge<TYPE> *e1, *e2, *e3, *e4, *e5, *e6;

            const Material<TYPE> *faceMaterial = facesMaterials != NULL ? &facesMaterials[facesMaterialsIndices[m_faces.size()]] : NULL;

            e1 = createEdge(indexV1, indexV2, verticesCoords, verticesColors, verticesTextCoords);
            e2 = createEdge(indexV2, indexV3, verticesCoords, verticesColors, verticesTextCoords);
            e3 = createEdge(indexV3, indexV1, verticesCoords, verticesColors, verticesTextCoords);

            e4 = createEdge(indexV2, indexV1, verticesCoords, verticesColors, verticesTextCoords);
            e5 = createEdge(indexV3, indexV2, verticesCoords, verticesColors, verticesTextCoords);
            e6 = createEdge(indexV1, indexV3, verticesCoords, verticesColors, verticesTextCoords);

            Face<TYPE> *newFace = new Face<TYPE>(m_vertices[indexV1], m_vertices[indexV2], m_vertices[indexV3],
                                                 e1, e2, e3, faceMaterial);

            e1->setF1(newFace);
            e4->setF2(newFace);

            e2->setF1(newFace);
            e5->setF2(newFace);

            e3->setF1(newFace);
            e6->setF2(newFace);

            m_vertices[indexV1]->addFace(newFace);
            m_vertices[indexV2]->addFace(newFace);
            m_vertices[indexV3]->addFace(newFace);

            m_faces.push_back(newFace);
        }

    protected:
        std::vector<Face<TYPE> *> m_faces;
        std::map<std::pair<GLuint, GLuint>, Edge<TYPE> *> m_edges;
        std::vector<Vertex<TYPE> *> m_vertices;
        bool m_containsVerticesTextCoords;
        bool m_containsVerticesColors;
        bool m_containsFacesMaterials;
    };
}

#endif // MESH_H
