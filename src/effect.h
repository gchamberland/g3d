#ifndef EFFECT_H
#define EFFECT_H

#include <GL/glew.h>

#include "types/g3dmatrix.h"
#include "types/g3darray.h"

#include "program.h"

namespace G3D
{
    class Effect
    {
        class GenericEffectParam
        {
        public:
            GenericEffectParam();
            virtual GenericEffectParam *clone() const = 0;
            virtual void update() const = 0;
        };


        template <typename TYPE>
        class EffectParam : public GenericEffectParam
        {
        public:
            EffectParam(GLuint location, const TYPE &value)
            {
                m_location = location;
                m_value = value;
            }

            EffectParam(const EffectParam<TYPE> &other)
            {
                m_location = other.m_location;
                m_value = other.m_value;
            }

            virtual GenericEffectParam *clone() const
            {
                return new EffectParam(*this);
            }

            virtual void update() const
            {
               setUniformValue(m_location, m_value);
            }

        private:
            GLuint m_location;
            TYPE m_value;
        };

    public:
        Effect(Program program)
        {
            m_program = program;
        }

        Effect(const Effect &other)
        {
            m_program = other.m_program;

            for (std::map<const std::string, GenericEffectParam *>::const_iterator it = other.m_params.begin();
                 it != other.m_params.end(); it++)
            {
                m_params.insert(std::make_pair(it->first, it->second->clone()));
            }
        }

        Effect()
        {
        }

        ~Effect()
        {
            for (std::map<const std::string, GenericEffectParam *>::iterator it = m_params.begin();
                 it != m_params.end(); it++)
            {
                delete it->second;
            }
        }

        template <typename TYPE>
        void setParam(const char *paramName, const TYPE &value)
        {
            GLenum glslType = glsl_type<TYPE>::value;
            const GLSLParam *param = m_program.getUniformInfos(paramName);

            switch (glslType)
            {
                case GL_FALSE:
                    throw std::runtime_error("EffectInstance.setParam() : Unregistered GLSL type !");
                break;

                default:
                    if (param->type != glslType || param->nbElem != 1)
                        throw std::runtime_error("EffectInstance.setParam() : No matching GLSL param !");
                break;
            }

            std::string str(paramName);

            if (m_params.find(str) != m_params.end())
                delete m_params.at(str);

            m_params[str] = new EffectParam<TYPE>(param->location, value);
        }

        template <typename TYPE, unsigned int N_ROWS, unsigned int N_COLS>
        void setParam(const char *paramName, const MatrixArray<TYPE, N_ROWS, N_COLS> &array)
        {
            GLenum glslType = glsl_type<typename Matrix<TYPE, N_ROWS, N_COLS>::type>::value;
            const GLSLParam *param = m_program.getUniformInfos(paramName);

            switch (glslType)
            {
                case GL_FALSE:
                    throw std::runtime_error("EffectInstance.setParam() : Unregistered GLSL type !");
                break;

                default:
                    if (param->type != glslType || param->nbElem != array.size())
                        throw std::runtime_error("EffectInstance.setParam() : No matching GLSL param !");
                break;
            }

            std::string str(paramName);

            if (m_params.find(str) != m_params.end())
                delete m_params.at(str);

            m_params[str] = new EffectParam<MatrixArray<TYPE, N_ROWS, N_COLS> >(param->location, array);
        }

        void use();
        static void useNoEffect();

    private:
        std::map<const std::string, GenericEffectParam *> m_params;
        Program m_program;

        static unsigned int m_textureUnitIndex;

        static void setUniformValue(GLuint location, unsigned int value);
        static void setUniformValue(GLuint location, int value);
        static void setUniformValue(GLuint location, float value);
        static void setUniformValue(GLuint location, double value);

        static void setUniformValue(GLuint location, const G3D::Matrix<unsigned int, 2, 1>::type &matrix);
        static void setUniformValue(GLuint location, const G3D::Matrix<unsigned int, 3, 1>::type &matrix);
        static void setUniformValue(GLuint location, const G3D::Matrix<unsigned int, 4, 1>::type &matrix);
        static void setUniformValue(GLuint location, const G3D::Matrix<int, 2, 1>::type &matrix);
        static void setUniformValue(GLuint location, const G3D::Matrix<int, 3, 1>::type &matrix);
        static void setUniformValue(GLuint location, const G3D::Matrix<int, 4, 1>::type &matrix);
        static void setUniformValue(GLuint location, const G3D::Matrix<float, 2, 1>::type &matrix);
        static void setUniformValue(GLuint location, const G3D::Matrix<float, 3, 1>::type &matrix);
        static void setUniformValue(GLuint location, const G3D::Matrix<float, 4, 1>::type &matrix);
        static void setUniformValue(GLuint location, const G3D::Matrix<double, 2, 1>::type &matrix);
        static void setUniformValue(GLuint location, const G3D::Matrix<double, 3, 1>::type &matrix);
        static void setUniformValue(GLuint location, const G3D::Matrix<double, 4, 1>::type &matrix);
        static void setUniformValue(GLuint location, const G3D::Matrix<float, 2, 2>::type &matrix);
        static void setUniformValue(GLuint location, const G3D::Matrix<float, 3, 3>::type &matrix);
        static void setUniformValue(GLuint location, const G3D::Matrix<float, 4, 4>::type &matrix);
        static void setUniformValue(GLuint location, const G3D::Matrix<float, 2, 3>::type &matrix);
        static void setUniformValue(GLuint location, const G3D::Matrix<float, 3, 2>::type &matrix);
        static void setUniformValue(GLuint location, const G3D::Matrix<float, 2, 4>::type &matrix);
        static void setUniformValue(GLuint location, const G3D::Matrix<float, 4, 2>::type &matrix);
        static void setUniformValue(GLuint location, const G3D::Matrix<float, 3, 4>::type &matrix);
        static void setUniformValue(GLuint location, const G3D::Matrix<float, 4, 3>::type &matrix);
        static void setUniformValue(GLuint location, const G3D::Matrix<double, 2, 2>::type &matrix);
        static void setUniformValue(GLuint location, const G3D::Matrix<double, 3, 3>::type &matrix);
        static void setUniformValue(GLuint location, const G3D::Matrix<double, 4, 4>::type &matrix);
        static void setUniformValue(GLuint location, const G3D::Matrix<double, 2, 3>::type &matrix);
        static void setUniformValue(GLuint location, const G3D::Matrix<double, 3, 2>::type &matrix);
        static void setUniformValue(GLuint location, const G3D::Matrix<double, 2, 4>::type &matrix);
        static void setUniformValue(GLuint location, const G3D::Matrix<double, 4, 2>::type &matrix);
        static void setUniformValue(GLuint location, const G3D::Matrix<double, 3, 4>::type &matrix);
        static void setUniformValue(GLuint location, const G3D::Matrix<double, 4, 3>::type &matrix);

        static void setUniformValue(GLuint location, const G3D::MatrixArray<unsigned int, 2, 1> &array);
        static void setUniformValue(GLuint location, const G3D::MatrixArray<unsigned int, 3, 1> &array);
        static void setUniformValue(GLuint location, const G3D::MatrixArray<unsigned int, 4, 1> &array);
        static void setUniformValue(GLuint location, const G3D::MatrixArray<int, 2, 1> &array);
        static void setUniformValue(GLuint location, const G3D::MatrixArray<int, 3, 1> &array);
        static void setUniformValue(GLuint location, const G3D::MatrixArray<int, 4, 1> &array);
        static void setUniformValue(GLuint location, const G3D::MatrixArray<float, 2, 1> &array);
        static void setUniformValue(GLuint location, const G3D::MatrixArray<float, 3, 1> &array);
        static void setUniformValue(GLuint location, const G3D::MatrixArray<float, 4, 1> &array);
        static void setUniformValue(GLuint location, const G3D::MatrixArray<double, 2, 1> &array);
        static void setUniformValue(GLuint location, const G3D::MatrixArray<double, 3, 1> &array);
        static void setUniformValue(GLuint location, const G3D::MatrixArray<double, 4, 1> &array);
        static void setUniformValue(GLuint location, const G3D::MatrixArray<float, 2, 2> &array);
        static void setUniformValue(GLuint location, const G3D::MatrixArray<float, 3, 3> &array);
        static void setUniformValue(GLuint location, const G3D::MatrixArray<float, 4, 4> &array);
        static void setUniformValue(GLuint location, const G3D::MatrixArray<float, 2, 3> &array);
        static void setUniformValue(GLuint location, const G3D::MatrixArray<float, 3, 2> &array);
        static void setUniformValue(GLuint location, const G3D::MatrixArray<float, 2, 4> &array);
        static void setUniformValue(GLuint location, const G3D::MatrixArray<float, 4, 2> &array);
        static void setUniformValue(GLuint location, const G3D::MatrixArray<float, 3, 4> &array);
        static void setUniformValue(GLuint location, const G3D::MatrixArray<float, 4, 3> &array);
        static void setUniformValue(GLuint location, const G3D::MatrixArray<double, 2, 2> &array);
        static void setUniformValue(GLuint location, const G3D::MatrixArray<double, 3, 3> &array);
        static void setUniformValue(GLuint location, const G3D::MatrixArray<double, 4, 4> &array);
        static void setUniformValue(GLuint location, const G3D::MatrixArray<double, 2, 3> &array);
        static void setUniformValue(GLuint location, const G3D::MatrixArray<double, 3, 2> &array);
        static void setUniformValue(GLuint location, const G3D::MatrixArray<double, 2, 4> &array);
        static void setUniformValue(GLuint location, const G3D::MatrixArray<double, 4, 2> &array);
        static void setUniformValue(GLuint location, const G3D::MatrixArray<double, 3, 4> &array);
        static void setUniformValue(GLuint location, const G3D::MatrixArray<double, 4, 3> &array);

        static void setUniformValue(GLuint location, const Texture &texture);
    };
}

#endif // EFFECT_H
