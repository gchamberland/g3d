#include "perspectivecamera.h"

#include <GL/glew.h>

#include <iostream>

namespace G3D
{
    PerspectiveCamera::PerspectiveCamera(const G3DWidget *widget, float fovy, float zNear, float zFar)
    {
        m_position.x() = 3;
        m_position.y() = 4;
        m_position.z() = 2;

        m_targetPoint.x() = 0;
        m_targetPoint.y() = 0;
        m_targetPoint.z() = 0;

        updateLookDirection();

        m_fovy = fovy;
        m_zNear = zNear;
        m_zFar = zFar;

        connectSlots(widget);
    }

    PerspectiveCamera::PerspectiveCamera(const G3DWidget *widget, float x, float y, float z,
                                         float targetPointX, float targetPointY, float targetPointZ,
                                         float fovy, float zNear, float zFar)
    {
        setPosition(x, y, z);
        setTargetPoint(targetPointX, targetPointY, targetPointZ);

        m_fovy = fovy;
        m_zNear = zNear;
        m_zFar = zFar;

        connectSlots(widget);
    }

    PerspectiveCamera::PerspectiveCamera(const G3DWidget *widget, const Vector3F &position, const Vector3F &targetPoint,
                                         float fovy, float zNear, float zFar)
    {
        setPosition(position);
        setTargetPoint(targetPoint);

        m_fovy = fovy;
        m_zNear = zNear;
        m_zFar = zFar;

        connectSlots(widget);
    }

    void PerspectiveCamera::setPosition(float x, float y, float z)
    {
        m_position.x() = x;
        m_position.y() = y;
        m_position.z() = z;
    }

    void PerspectiveCamera::setPosition(const Vector3F &newPosition)
    {
        m_position = newPosition;
    }

    float PerspectiveCamera::getX() const
    {
        return m_position.x();
    }

    float PerspectiveCamera::getY() const
    {
        return m_position.y();
    }

    float PerspectiveCamera::getZ() const
    {
        return m_position.z();
    }

    const Vector3F &PerspectiveCamera::getPosition() const
    {
        return m_position;
    }

    void PerspectiveCamera::setTargetPoint(float x, float y, float z)
    {
        m_targetPoint.x() = x;
        m_targetPoint.y() = y;
        m_targetPoint.z() = z;

        updateLookDirection();
    }

    void PerspectiveCamera::setTargetPoint(const Vector3F &newTargetPoint)
    {
        m_targetPoint = newTargetPoint;

        updateLookDirection();
    }

    float PerspectiveCamera::getTargetPointX() const
    {
        return m_targetPoint.x();
    }

    float PerspectiveCamera::getTargetPointY() const
    {
        return m_targetPoint.y();
    }

    float PerspectiveCamera::getTargetPointZ() const
    {
        return m_targetPoint.z();
    }

    const Vector3F &PerspectiveCamera::getTargetPoint() const
    {
        return m_targetPoint;
    }

    void PerspectiveCamera::setLookDirection(float x, float y, float z)
    {
        m_lookDirection.x() = x;
        m_lookDirection.y() = y;
        m_lookDirection.z() = z;

        updateTargetPoint();
    }

    void PerspectiveCamera::setLookDirection(const Vector3F &newLookDirection)
    {
        m_lookDirection = newLookDirection;

        updateTargetPoint();
    }

    float PerspectiveCamera::getLookDirectionX() const
    {
        return m_lookDirection.x();
    }

    float PerspectiveCamera::getLookDirectionY() const
    {
        return m_lookDirection.y();
    }

    float PerspectiveCamera::getLookDirectionZ() const
    {
        return m_lookDirection.z();
    }

    const Vector3F &PerspectiveCamera::getLookDirection() const
    {
        return m_lookDirection;
    }

    float PerspectiveCamera::getFovy()
    {
        return m_fovy;
    }

    float PerspectiveCamera::getZNear()
    {
        return m_zNear;
    }

    float PerspectiveCamera::getZFar()
    {
        return m_zFar;
    }

    void PerspectiveCamera::update() const
    {
        glMatrixMode( GL_MODELVIEW );
        glLoadIdentity( );

        gluLookAt(m_position.x(), m_position.y(), m_position.z(), m_targetPoint.x(), m_targetPoint.y(), m_targetPoint.z(), 0, 1, 0);
    }

    void PerspectiveCamera::sizeChangedSlot(int width, int height)
    {
        glMatrixMode( GL_PROJECTION );
        glLoadIdentity();

        gluPerspective(m_fovy, ((float) width) / height, m_zNear, m_zFar);
    }

    void PerspectiveCamera::updateLookDirection()
    {
        m_lookDirection = m_targetPoint - m_position;
        m_lookDirection.normalize();
    }

    void PerspectiveCamera::updateTargetPoint()
    {
        m_targetPoint = m_position + m_lookDirection;
    }

    void PerspectiveCamera::connectSlots(const G3DWidget *widget)
    {
        connect(widget, SIGNAL(sizeChanged(int,int)), this, SLOT(sizeChangedSlot(int, int)));
    }
}
