#ifndef TEXTURE_H
#define TEXTURE_H

#include <string>
#include <map>

#include <GL/glew.h>

class Texture
{
public:
    Texture();
    Texture(const Texture &other);
    Texture(const char *fileName);
    Texture(int width, int height, GLenum format, GLenum internalFormat, GLenum glType = GL_UNSIGNED_BYTE, float *data = NULL);
    ~Texture();
    void use(unsigned int textureUnitIndex = 0) const;
    static void useNoTexture(unsigned int textureUnitIndex = 0);

private:
    GLuint m_id;
    std::map<GLuint, unsigned int> m_textureInstances;
};

#endif // TEXTURE_H
