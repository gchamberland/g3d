#ifndef MODEL3D_H
#define MODEL3D_H

#include "g3dUtils.h"
#include "object3d.h"

class Object3D;

class Model3D
{
public:
    Model3D();

private:
    std::vector<Material<float> > m_materials;
    std::map<const std::string, Object3D> m_objects;
};

#endif // MODEL3D_H
