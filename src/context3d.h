#ifndef CONTEXT3D_H
#define CONTEXT3D_H

#include <GL/glew.h>

#include <QGLWidget>

#include "perspectivecamera.h"

namespace G3D
{
    class PerspectiveCamera;

    class G3DWidget : public QGLWidget
    {
        Q_OBJECT

    public:
        explicit G3DWidget(int framesPerSecond = 0, QWidget *parent = 0, char *name = 0);
        ~G3DWidget();

        virtual void initializeGL();
        virtual void resizeGL(int width, int height);
        virtual void paintGL();
        virtual void keyPressEvent( QKeyEvent *keyEvent );

        static bool isSupported(const char *extensionName);

    public slots:
        virtual void timeOutSlot();

    signals:
        void sizeChanged(int, int);

    private:
        const PerspectiveCamera *getCamera();
        QTimer *m_timer;
        PerspectiveCamera *m_camera;
        static QStringList *m_extensions;
    };
}

#endif // CONTEXT3D_H
