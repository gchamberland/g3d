#include "effect.h"

namespace G3D
{
    unsigned int Effect::m_textureUnitIndex = 0;

    void Effect::use()
    {
        for (std::map<const std::string, GenericEffectParam *>::iterator it = m_params.begin();
             it != m_params.end(); it++)
            it->second->update();

        glUseProgram(m_program.getProgramId());
    }

    void Effect::useNoEffect()
    {
        while (m_textureUnitIndex > 0)
        {
            Texture::useNoTexture(m_textureUnitIndex--);
        }

        glUseProgram(0);
    }

    void Effect::setUniformValue(GLuint location, unsigned int value)
    {
        glUniform1ui(location, value);
    }

    void Effect::setUniformValue(GLuint location, int value)
    {
        glUniform1i(location, value);
    }

    void Effect::setUniformValue(GLuint location, float value)
    {
        glUniform1f(location, value);
    }

    void Effect::setUniformValue(GLuint location, double value)
    {
        glUniform1d(location, value);
    }

    void Effect::setUniformValue(GLuint location, const G3D::Matrix<unsigned int, 2, 1>::type &matrix)
    {
        glUniform2uiv(location, 1, matrix.data());
    }

    void Effect::setUniformValue(GLuint location, const G3D::Matrix<unsigned int, 3, 1>::type &matrix)
    {
        glUniform3uiv(location, 1, matrix.data());
    }

    void Effect::setUniformValue(GLuint location, const G3D::Matrix<unsigned int, 4, 1>::type &matrix)
    {
        glUniform4uiv(location, 1, matrix.data());
    }

    void Effect::setUniformValue(GLuint location, const G3D::Matrix<int, 2, 1>::type &matrix)
    {
        glUniform2iv(location, 1, matrix.data());
    }

    void Effect::setUniformValue(GLuint location, const G3D::Matrix<int, 3, 1>::type &matrix)
    {
        glUniform3iv(location, 1, matrix.data());
    }

    void Effect::setUniformValue(GLuint location, const G3D::Matrix<int, 4, 1>::type &matrix)
    {
        glUniform4iv(location, 1, matrix.data());
    }

    void Effect::setUniformValue(GLuint location, const G3D::Matrix<float, 2, 1>::type &matrix)
    {
        glUniform2fv(location, 1, matrix.data());
    }

    void setUniformValue(GLuint location, const G3D::Matrix<float, 3, 1>::type &matrix)
    {
        glUniform3fv(location, 1, matrix.data());
    }

    void Effect::setUniformValue(GLuint location, const G3D::Matrix<float, 4, 1>::type &matrix)
    {
        glUniform4fv(location, 1, matrix.data());
    }

    void Effect::setUniformValue(GLuint location, const G3D::Matrix<double, 2, 1>::type &matrix)
    {
        glUniform2dv(location, 1, matrix.data());
    }

    void Effect::setUniformValue(GLuint location, const G3D::Matrix<double, 3, 1>::type &matrix)
    {
        glUniform3dv(location, 1, matrix.data());
    }

    void Effect::setUniformValue(GLuint location, const G3D::Matrix<double, 4, 1>::type &matrix)
    {
        glUniform4dv(location, 1, matrix.data());
    }

    void Effect::setUniformValue(GLuint location, const G3D::Matrix<float, 2, 2>::type &matrix)
    {
        glUniformMatrix2fv(location, 1, GL_FALSE, matrix.data());
    }

    void Effect::setUniformValue(GLuint location, const G3D::Matrix<float, 3, 3>::type &matrix)
    {
        glUniformMatrix3fv(location, 1, GL_FALSE, matrix.data());
    }

    void Effect::setUniformValue(GLuint location, const G3D::Matrix<float, 4, 4>::type &matrix)
    {
        glUniformMatrix4fv(location, 1, GL_FALSE, matrix.data());
    }

    void Effect::setUniformValue(GLuint location, const G3D::Matrix<float, 2, 3>::type &matrix)
    {
        glUniformMatrix2x3fv(location, 1, GL_FALSE, matrix.data());
    }

    void Effect::setUniformValue(GLuint location, const G3D::Matrix<float, 3, 2>::type &matrix)
    {
        glUniformMatrix3x2fv(location, 1, GL_FALSE, matrix.data());
    }

    void Effect::setUniformValue(GLuint location, const G3D::Matrix<float, 2, 4>::type &matrix)
    {
        glUniformMatrix2x4fv(location, 1, GL_FALSE, matrix.data());
    }

    void Effect::setUniformValue(GLuint location, const G3D::Matrix<float, 4, 2>::type &matrix)
    {
        glUniformMatrix4x2fv(location, 1, GL_FALSE, matrix.data());
    }

    void Effect::setUniformValue(GLuint location, const G3D::Matrix<float, 3, 4>::type &matrix)
    {
        glUniformMatrix3x4fv(location, 1, GL_FALSE, matrix.data());
    }

    void Effect::setUniformValue(GLuint location, const G3D::Matrix<float, 4, 3>::type &matrix)
    {
        glUniformMatrix4x3fv(location, 1, GL_FALSE, matrix.data());
    }

    void Effect::setUniformValue(GLuint location, const G3D::Matrix<double, 2, 2>::type &matrix)
    {
        glUniformMatrix2dv(location, 1, GL_FALSE, matrix.data());
    }

    void Effect::setUniformValue(GLuint location, const G3D::Matrix<double, 3, 3>::type &matrix)
    {
        glUniformMatrix3dv(location, 1, GL_FALSE, matrix.data());
    }

    void Effect::setUniformValue(GLuint location, const G3D::Matrix<double, 4, 4>::type &matrix)
    {
        glUniformMatrix4dv(location, 1, GL_FALSE, matrix.data());
    }

    void Effect::setUniformValue(GLuint location, const G3D::Matrix<double, 2, 3>::type &matrix)
    {
        glUniformMatrix2x3dv(location, 1, GL_FALSE, matrix.data());
    }

    void Effect::setUniformValue(GLuint location, const G3D::Matrix<double, 3, 2>::type &matrix)
    {
        glUniformMatrix3x2dv(location, 1, GL_FALSE, matrix.data());
    }

    void Effect::setUniformValue(GLuint location, const G3D::Matrix<double, 2, 4>::type &matrix)
    {
        glUniformMatrix2x4dv(location, 1, GL_FALSE, matrix.data());
    }

    void Effect::setUniformValue(GLuint location, const G3D::Matrix<double, 4, 2>::type &matrix)
    {
        glUniformMatrix4x2dv(location, 1, GL_FALSE, matrix.data());
    }

    void Effect::setUniformValue(GLuint location, const G3D::Matrix<double, 3, 4>::type &matrix)
    {
        glUniformMatrix3x4dv(location, 1, GL_FALSE, matrix.data());
    }

    void Effect::setUniformValue(GLuint location, const G3D::Matrix<double, 4, 3>::type &matrix)
    {
        glUniformMatrix4x3dv(location, 1, GL_FALSE, matrix.data());
    }

    void Effect::setUniformValue(GLuint location, const G3D::MatrixArray<unsigned int, 2, 1> &array)
    {
        glUniform2uiv(location, array.size(), array.data());
    }

    void Effect::setUniformValue(GLuint location, const G3D::MatrixArray<unsigned int, 3, 1> &array)
    {
        glUniform3uiv(location, array.size(), array.data());
    }

    void Effect::setUniformValue(GLuint location, const G3D::MatrixArray<unsigned int, 4, 1> &array)
    {
        glUniform4uiv(location, array.size(), array.data());
    }

    void Effect::setUniformValue(GLuint location, const G3D::MatrixArray<int, 2, 1> &array)
    {
        glUniform2iv(location, array.size(), array.data());
    }

    void Effect::setUniformValue(GLuint location, const G3D::MatrixArray<int, 3, 1> &array)
    {
        glUniform3iv(location, array.size(), array.data());
    }

    void Effect::setUniformValue(GLuint location, const G3D::MatrixArray<int, 4, 1> &array)
    {
        glUniform4iv(location, array.size(), array.data());
    }

    void Effect::setUniformValue(GLuint location, const G3D::MatrixArray<float, 2, 1> &array)
    {
        glUniform2fv(location, array.size(), array.data());
    }

    void Effect::setUniformValue(GLuint location, const G3D::MatrixArray<float, 3, 1> &array)
    {
        glUniform3fv(location, array.size(), array.data());
    }

    void Effect::setUniformValue(GLuint location, const G3D::MatrixArray<float, 4, 1> &array)
    {
        glUniform4fv(location, array.size(), array.data());
    }

    void Effect::setUniformValue(GLuint location, const G3D::MatrixArray<double, 2, 1> &array)
    {
        glUniform2dv(location, array.size(), array.data());
    }

    void Effect::setUniformValue(GLuint location, const G3D::MatrixArray<double, 3, 1> &array)
    {
        glUniform3dv(location, array.size(), array.data());
    }

    void Effect::setUniformValue(GLuint location, const G3D::MatrixArray<double, 4, 1> &array)
    {
        glUniform4dv(location, array.size(), array.data());
    }

    void Effect::setUniformValue(GLuint location, const G3D::MatrixArray<float, 2, 2> &array)
    {
        glUniformMatrix2fv(location, array.size(), GL_FALSE, array.data());
    }

    void Effect::setUniformValue(GLuint location, const G3D::MatrixArray<float, 3, 3> &array)
    {
        glUniformMatrix3fv(location, array.size(), GL_FALSE, array.data());
    }

    void Effect::setUniformValue(GLuint location, const G3D::MatrixArray<float, 4, 4> &array)
    {
        glUniformMatrix4fv(location, array.size(), GL_FALSE, array.data());
    }

    void Effect::setUniformValue(GLuint location, const G3D::MatrixArray<float, 2, 3> &array)
    {
        glUniformMatrix2x3fv(location, array.size(), GL_FALSE, array.data());
    }

    void Effect::setUniformValue(GLuint location, const G3D::MatrixArray<float, 3, 2> &array)
    {
        glUniformMatrix3x2fv(location, array.size(), GL_FALSE, array.data());
    }

    void Effect::setUniformValue(GLuint location, const G3D::MatrixArray<float, 2, 4> &array)
    {
        glUniformMatrix2x4fv(location, array.size(), GL_FALSE, array.data());
    }

    void Effect::setUniformValue(GLuint location, const G3D::MatrixArray<float, 4, 2> &array)
    {
        glUniformMatrix4x2fv(location, array.size(), GL_FALSE, array.data());
    }

    void Effect::setUniformValue(GLuint location, const G3D::MatrixArray<float, 3, 4> &array)
    {
        glUniformMatrix3x4fv(location, array.size(), GL_FALSE, array.data());
    }

    void Effect::setUniformValue(GLuint location, const G3D::MatrixArray<float, 4, 3> &array)
    {
        glUniformMatrix4x3fv(location, array.size(), GL_FALSE, array.data());
    }

    void Effect::setUniformValue(GLuint location, const G3D::MatrixArray<double, 2, 2> &array)
    {
        glUniformMatrix2dv(location, array.size(), GL_FALSE, array.data());
    }

    void Effect::setUniformValue(GLuint location, const G3D::MatrixArray<double, 3, 3> &array)
    {
        glUniformMatrix3dv(location, array.size(), GL_FALSE, array.data());
    }

    void Effect::setUniformValue(GLuint location, const G3D::MatrixArray<double, 4, 4> &array)
    {
        glUniformMatrix4dv(location, array.size(), GL_FALSE, array.data());
    }

    void Effect::setUniformValue(GLuint location, const G3D::MatrixArray<double, 2, 3> &array)
    {
        glUniformMatrix2x3dv(location, array.size(), GL_FALSE, array.data());
    }

    void Effect::setUniformValue(GLuint location, const G3D::MatrixArray<double, 3, 2> &array)
    {
        glUniformMatrix3x2dv(location, array.size(), GL_FALSE, array.data());
    }

    void Effect::setUniformValue(GLuint location, const G3D::MatrixArray<double, 2, 4> &array)
    {
        glUniformMatrix2x4dv(location, array.size(), GL_FALSE, array.data());
    }

    void Effect::setUniformValue(GLuint location, const G3D::MatrixArray<double, 4, 2> &array)
    {
        glUniformMatrix4x2dv(location, array.size(), GL_FALSE, array.data());
    }

    void Effect::setUniformValue(GLuint location, const G3D::MatrixArray<double, 3, 4> &array)
    {
        glUniformMatrix3x4dv(location, array.size(), GL_FALSE, array.data());
    }

    void Effect::setUniformValue(GLuint location, const G3D::MatrixArray<double, 4, 3> &array)
    {
        glUniformMatrix4x3dv(location, array.size(), GL_FALSE, array.data());
    }

    void Effect::setUniformValue(GLuint location, const Texture &texture)
    {
        texture.use(m_textureUnitIndex);
        glUniform1ui(location, m_textureUnitIndex++);
    }
}
