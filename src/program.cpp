#include "program.h"

#include <iostream>
#include <sstream>

#include "context3d.h"

namespace G3D
{
Program::Program()
{
    m_id = 0;
}

Program::Program(const char *vertexShaderFileName,
                   const char *fragmentShaderFileName,
                   const char *geometryShaderFileName,
                   const char *tessellationControlShaderFileName,
                   const char *tessellationEvalutaionShaderFileName)
    {
        if (!(G3DWidget::isSupported("GL_ARB_shader_objects") &&
              G3DWidget::isSupported("GL_ARB_shading_language_100") &&
              G3DWidget::isSupported("GL_ARB_vertex_shader")))
        {
            throw std::runtime_error("Impossible to use shader on this implementation : need at least "
                                     "GL_ARB_shader_objects, GL_ARB_shading_language_100, GL_ARB_vertex_shader extensions support.");
        }

        m_shadersIds.push_back(loadShader(GL_VERTEX_SHADER, vertexShaderFileName));

        if (fragmentShaderFileName != NULL)
        {
            if (!G3DWidget::isSupported("GL_ARB_fragment_shader"))
                throw std::runtime_error("Impossible to use fragment shaders on this implementation : "
                                         "GL_ARB_fragment_shader extension unsupported");

            m_shadersIds.push_back(loadShader(GL_FRAGMENT_SHADER, fragmentShaderFileName));
        }

        if (geometryShaderFileName != NULL)
        {
            if (!G3DWidget::isSupported("GL_ARB_geometry_shader4"))
                throw std::runtime_error("Impossible to use geometry shaders on this implementation : "
                                         "GL_ARB_geometry_shader4 extension unsupported");

            m_shadersIds.push_back(loadShader(GL_GEOMETRY_SHADER_ARB, geometryShaderFileName));
        }

        if (tessellationControlShaderFileName != NULL)
        {
            if (!G3DWidget::isSupported("GL_ARB_tessellation_shader"))
                throw std::runtime_error("Impossible to use tesselation shaders on this implementation : "
                                         "GL_ARB_tessellation_shader extension unsupported");

            m_shadersIds.push_back(loadShader(GL_TESS_CONTROL_SHADER, tessellationControlShaderFileName));
        }

        if (tessellationEvalutaionShaderFileName != NULL)
        {
            if (!G3DWidget::isSupported("GL_ARB_tessellation_shader"))
                throw std::runtime_error("Impossible to use tesselation shaders on this implementation : "
                                         "GL_ARB_tessellation_shader extension unsupported");

            m_shadersIds.push_back(loadShader(GL_TESS_EVALUATION_SHADER, tessellationEvalutaionShaderFileName));
        }

        m_id = glCreateProgram();

        if (m_id == 0)
        {
            throw std::runtime_error("Impossible to create a new shader program.");
        }

        attachShaders(m_id, m_shadersIds);

        listParams<GL_ACTIVE_ATTRIBUTES>(m_id, &m_attributs);
        listParams<GL_ACTIVE_UNIFORMS>(m_id, &m_uniforms);

        m_programInstances[m_id] = 1;
}

Program::Program(const Program &other)
{
    if (other.m_id != 0)
    {
        m_id = other.m_id;
        m_shadersIds = other.m_shadersIds;
        m_attributs = other.m_attributs;
        m_uniforms = other.m_uniforms;

        m_programInstances[m_id]++;
    }
}

    Program::~Program()
    {
        m_programInstances[m_id]--;

        if (m_programInstances[m_id] == 0)
        {
            for (int i=0; i<m_shadersIds.size(); i++)
                glDeleteShader(m_shadersIds.at(i));

            glDeleteProgram(m_id);
        }
    }

    void Program::attachShaders(GLuint programId, const std::vector<GLuint> &shadersIds)
    {
        GLint linkStatus;

        for (int i=0; i<shadersIds.size(); i++)
            glAttachShader(programId, shadersIds[i]);

        glLinkProgram(programId);

        glGetProgramiv(programId, GL_LINK_STATUS, &linkStatus);

        if (linkStatus != GL_TRUE)
        {
            GLint logSize;
            char *log;
            std::stringstream str;

            glGetProgramiv(programId, GL_INFO_LOG_LENGTH, &logSize);

            log = new char[logSize+1];

            if(log == NULL)
            {
                throw std::runtime_error("Program linking error : out of memory");
            }

            memset(log, '\0', logSize + 1);

            glGetProgramInfoLog(programId, logSize, &logSize, log);

            str << "Program linking error :\n" << log;

            delete log;

            throw std::runtime_error(str.str());
        }
    }

    GLuint Program::getProgramId() const
    {
        return m_id;
    }

    bool Program::hasAttribut(const char *varName) const
    {
        return m_attributs.find(varName) != m_attributs.end();
    }

    bool Program::hasUniform(const char *varName) const
    {
        return m_uniforms.find(varName) != m_uniforms.end();
    }

    const GLSLParam * Program::getUniformInfos(const char *varName) const
    {
        if (m_uniforms.find(varName) != m_uniforms.end())
            return &m_uniforms.at(varName);
        else
        {
            std::stringstream str;

            str << "Impossible de trouver la variable \"" << varName << "\" dans le programme.\n";
            str << "Il est possible que la variable ai été optimisé par le compilateur glsl si elle\n";
            str << "est inutile pour le rendu.";

            throw std::runtime_error(str.str());
        }
    }

    const GLSLParam * Program::getAttributInfos(const char *varName) const
    {
        if (m_attributs.find(varName) != m_attributs.end())
            return &m_attributs.at(varName);
        else
        {
            std::stringstream str;

            str << "Impossible de trouver la variable \"" << varName << "\" dans le programme.\n";
            str << "Il est possible que la variable ai été optimisé par le compilateur glsl si elle\n";
            str << "est inutile pour le rendu.";

            throw std::runtime_error(str.str());
        }
    }

    GLuint Program::loadShader(GLenum type, const char *filename)
    {
        char *sources = NULL;
        GLint compileStatus;
        GLuint shaderId = glCreateShader(type);

        if(shaderId == 0)
        {
            std::stringstream str;

            str << "Impossible to create shader :\n" << gluErrorString(glGetError());

            throw std::runtime_error(str.str());
        }

        try
        {
            sources = loadSource(filename);
        }
        catch (std::runtime_error e)
        {
            std::stringstream str;

            str << "Impossible to load shader " << filename << " :\n->" << e.what();

            glDeleteShader(shaderId);

            throw std::runtime_error(str.str());
        }

        glShaderSource(shaderId, 1, (const GLchar**)&sources, NULL);

        glCompileShader(shaderId);

        delete sources;

        glGetShaderiv(shaderId, GL_COMPILE_STATUS, &compileStatus);

        if (compileStatus != GL_TRUE)
        {
            GLint logsize;
            char *log = NULL;
            std::stringstream str;

            glGetShaderiv(shaderId, GL_INFO_LOG_LENGTH, &logsize);

            /* on alloue un espace memoire dans lequel OpenGL ecrira le message */
            log = new char [logsize + 1];
            if(log == NULL)
            {
                throw std::runtime_error("Compilation error : out of memory.");
            }
            /* initialisation du contenu */
            memset(log, '\0', logsize + 1);

            glGetShaderInfoLog(shaderId, logsize, &logsize, log);

            str << "Compilation error(s) in file " << filename << " :\n" << log;

            /* ne pas oublier de liberer la memoire et notre shader */
            delete log;
            glDeleteShader(shaderId);

            throw std::runtime_error(str.str());
        }

        return shaderId;
    }

    char* Program::loadSource(const char *filename)
    {
        char *src = NULL;   /* code source de notre shader */
        FILE *fp = NULL;    /* fichier */
        long size;          /* taille du fichier */
        long i;             /* compteur */


        /* on ouvre le fichier */
        fp = fopen(filename, "r");
        /* on verifie si l'ouverture a echoue */
        if(fp == NULL)
        {
            throw std::runtime_error("Impossible d'ouvrir le fichier");
        }

        /* on recupere la longueur du fichier */
        fseek(fp, 0, SEEK_END);
        size = ftell(fp);

        /* on se replace au debut du fichier */
        rewind(fp);

        /* on alloue de la memoire pour y placer notre code source */
        src = new char [size+1]; /* +1 pour le caractere de fin de chaine '\0' */
        if(src == NULL)
        {
            fclose(fp);
            throw std::runtime_error("Erreur d'allocation mémoire.");
        }

        /* lecture du fichier */
        for(i=0; i<size; i++)
            src[i] = fgetc(fp);

        /* on place le dernier caractere a '\0' */
        src[size] = '\0';

        fclose(fp);

        return src;
    }

    std::map<GLuint, unsigned int> Program::m_programInstances;
}
