#include "context3d.h"

#include <iostream>
#include <stdexcept>

#include <QTimer>
#include <QKeyEvent>

#include "types/g3darray.h"
#include "effect.h"

namespace G3D
{
    QStringList *G3DWidget::m_extensions = NULL;

    G3DWidget::G3DWidget(int framesPerSecond, QWidget *parent, char *name)
    {
        setWindowTitle(QString::fromUtf8(name));

        if(framesPerSecond == 0)
            m_timer = NULL;
        else
        {
            int seconde = 1000; // 1 seconde = 1000 ms
            int timerInterval = seconde / framesPerSecond;

            m_timer = new QTimer(this);

            connect(m_timer, SIGNAL(timeout()), this, SLOT(timeOutSlot()));

            m_timer->start( timerInterval );
        }

        m_camera = NULL;
    }

    G3DWidget::~G3DWidget()
    {
        delete m_timer;

        if (m_camera != NULL)
            delete m_camera;

        if (m_extensions != NULL)
            delete m_extensions;
    }

    void G3DWidget::initializeGL()
    {
        glewExperimental = GL_TRUE;

        if (glewInit() != GLEW_OK)
            std::runtime_error("Impossible to load OpenGL extensions");

        if (m_extensions == NULL)
            m_extensions = new QStringList(QString((const char *)glGetString(GL_EXTENSIONS)).split(" "));

        m_camera = new PerspectiveCamera(this);
    }

    void G3DWidget::resizeGL(int width, int height)
    {
        emit sizeChanged(width, height);
    }

    void G3DWidget::paintGL()
    {
    }

    void G3DWidget::keyPressEvent(QKeyEvent *keyEvent)
    {
        switch(keyEvent->key())
       {
           case Qt::Key_Escape:
               close();
           break;
        }
    }

    bool G3DWidget::isSupported(const char *extensionName)
    {
        return m_extensions->contains(QString(extensionName));
    }

    void G3DWidget::timeOutSlot()
    {
        updateGL();
    }
}
