#include <iostream>

#include "context3d.h"

#include <QApplication>

int main(int argc, char **argv)
{
    QApplication app(argc, argv);

    G3D::G3DWidget G3D(40, 0, "G3D");
    G3D.show();

    return app.exec();
}
