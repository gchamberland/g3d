#ifndef FRAMEBUFFER_H
#define FRAMEBUFFER_H

#include <vector>
#include <stdexcept>

#include <GL/glew.h>
#include "texture.h"
#include "perspectivecamera.h"

template <int NB_COLOR_BUFFERS, bool DEPTH_IN_TEXTURE>
class FrameBuffer
{
public:
    FrameBuffer(int width, int height, const PerspectiveCamera *camera)
    {
        glGenFramebuffers(1, &m_id);

        m_instances[m_id] = 1;

        for (int i=0; i<NB_COLOR_BUFFERS; i++)
            m_colorBuffers.push_back(Texture(width, height, GL_RGBA, GL_RGBA32F, GL_FLOAT));

        if (DEPTH_IN_TEXTURE)
            m_colorBuffers.push_back(Texture(width, height, GL_DEPTH_COMPONENT, GL_DEPTH_COMPONENT, GL_FLOAT));
        else
            creerRenderBuffer(&m_renderBufferId, width, height, GL_DEPTH24_STENCIL8);

        m_width = width;
        m_height = height;
        m_camera = camera;

        glBindFramebuffer(GL_FRAMEBUFFER, m_id);

            for (int i=0; i<NB_COLOR_BUFFERS; i++)
            {
                glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0+i, GL_TEXTURE_2D, m_colorBuffers[i]->getId(), 0);
            }

            if (DEPTH_IN_TEXTURE)
                glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, m_colorBuffers[NB_COLOR_BUFFERS]->getId(), 0);
            else
                glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, m_renderBufferId);

            if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
            {
                switch (glCheckFramebufferStatus(GL_FRAMEBUFFER))
                {
                    case GL_FRAMEBUFFER_INCOMPLETE_ATTACHMENT :
                        throw std::runtime_error("Erreur : incomplete attachement !");
                    break;

                    case GL_FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT :
                        throw std::runtime_error("Erreur : missing attachement !");
                    break;

                    case GL_FRAMEBUFFER_UNDEFINED :
                        throw std::runtime_error("Erreur : Framebuffer undefined !");
                    break;

                    case GL_FRAMEBUFFER_UNSUPPORTED :
                        throw std::runtime_error("Erreur : Framebuffer unsupported !");
                    break;

                    case GL_FRAMEBUFFER_INCOMPLETE_DRAW_BUFFER :
                        throw std::runtime_error("Erreur : Framebuffer have an incomplete draw buffer !");
                    break;

                    default:
                        throw std::runtime_error("Erreur : framebuffer corrupted !");
                    break;
                }
            }

        glBindFramebuffer(GL_FRAMEBUFFER, 0);
    }

    FrameBuffer(const FrameBuffer &other)
    {
        m_id = other.m_id;

        m_instances[m_id]++;

        if (!DEPTH_IN_TEXTURE)
            m_renderBufferId = other.m_renderBufferId;

        m_width = other.m_width;
        m_height = other.m_height;
        m_camera = other.m_camera;

        m_colorBuffers = other.m_colorBuffers;
    }

    ~FrameBuffer()
    {
        m_instances[m_id]--;

        if (m_instances[m_id] == 0)
        {
            glDeleteFramebuffers(1, &m_id);

            if (DEPTH_IN_TEXTURE)
                glDeleteRenderbuffers(1, &m_renderBufferId);

            for (int i=0; i<m_colorBuffers.size(); i++)
                delete m_colorBuffers[i];
        }
    }

    void record()
    {
        glBindFramebuffer(GL_FRAMEBUFFER, m_id);
        glClearColor(0.0, 0.0, 0.0, 0.0);

        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT |  GL_STENCIL_BUFFER_BIT);

        glPushAttrib(GL_TRANSFORM_BIT | GL_VIEWPORT_BIT);
        glViewport(0, 0, m_width, m_height);
        glMatrixMode(GL_PROJECTION);
        glPushMatrix();
        glLoadIdentity();
        gluPerspective( 60.0, (((float) m_width) / m_height), 1.0, 1024.0 );

        glMatrixMode(GL_MODELVIEW);
        glPushMatrix();

        m_camera->update();
    }

    void stopRecording()
    {
        glMatrixMode(GL_MODELVIEW);
        glPopMatrix();

        glMatrixMode(GL_PROJECTION);
        glPopMatrix();

        glPopAttrib();

        glBindFramebuffer(GL_FRAMEBUFFER, 0);
    }

    const Texture &getColorBuffer(int index)
    {
        if (index >= NB_COLOR_BUFFERS)
            throw std::runtime_error("Error : color buffer index out of bound !");

        return m_colorBuffers[index];
    }

    const Texture &getDepthBuffer()
    {
        if (!DEPTH_IN_TEXTURE)
            throw std::runtime_error("FrameBuffer error : template parameter "
                                     "DEPTH_IN_TEXTURE must be set at true to acces the depth buffer");

        return m_colorBuffers[NB_COLOR_BUFFERS];
    }

private:
    int m_width, m_height;
    const PerspectiveCamera *m_camera;
    GLuint m_id;
    GLuint m_renderBufferId;
    std::vector<Texture> m_colorBuffers;

    static std::map<GLuint, unsigned int> m_instances;

    static void creerRenderBuffer(GLuint *id, int width, int height, GLenum internalFormat)
    {
        glGenRenderbuffers(1, id);

        glBindRenderbuffer(GL_RENDERBUFFER, *id);

            glRenderbufferStorage(GL_RENDERBUFFER, internalFormat, width, height);

        glBindRenderbuffer(GL_RENDERBUFFER, 0);
    }
};

#endif // FRAMEBUFFER_H
