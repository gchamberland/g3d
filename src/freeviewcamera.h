#ifndef FREEVIEWCAMERA_H
#define FREEVIEWCAMERA_H

#include "perspectivecamera.h"

namespace G3D
{
    class FreeViewCamera : public PerspectiveCamera
    {
    public:
        FreeViewCamera(const G3DWidget *widget);
        void forward();

        void horizontaleRotation(float angle);
        void verticaleRotation(float angle);

        void forward(double acceleration);
        void backward(double acceleration);

        void right(double acceleration);
        void left(double acceleration);

        virtual void update();
    private:
        float m_speed;
        float m_lateraleSpeed;

        float m_horizontaleRotation;
        float m_verticaleRotation;
    };
}

#endif // FREEVIEWCAMERA_H
